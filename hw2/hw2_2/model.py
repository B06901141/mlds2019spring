import numpy as np
import os
import pickle
import tqdm

from gensim.models.word2vec import Word2Vec

import tensorflow as tf

import keras
from keras.models import Model
from keras.callbacks import ModelCheckpoint
from keras.layers import Input, GRU, Dense, Embedding, LSTM, Dropout, BatchNormalization, CuDNNGRU,LeakyReLU
from keras.utils import to_categorical
import keras.backend as K
from keras.models import load_model
from keras.utils import plot_model

#gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.75)
#sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
#K.set_session(sess)

import preprocess
import reader

from toSentence import *
from beam_search import *

class chineseChatbot(Model):
    """
    To create a model, you have to pass at least the following params:
    -w2v_model: a gensim word2vec model object
    -max_ind: the largest index in dictionary "to_index"
    -to_index: a dictionary from the words in y_train to index, this may different from that of w2v_model
    _YOU DON'T NEED TO PASS ANY OTHER PARAMS. They will be automatically passed by the system.

    TO load a model, run the following code.
    model = load_model("model.hdf5",custom_objects={'chineseChatbot': chineseChatbot})
    model.w2v_model = w2v_model
    model.max_ind = max_ind
    model.init(to_index)
    """
    """
    data structure:
    -index2vec: a dictionary which map int into vector (y_train -> vector)
    -lookup_vector: a lambda function utilize index2vec, return zero vector if DNE
    -encodeLayer/decodeLayer contain encoder/decoder intput layer and the embedding layer
    """
    def __init__(self,w2v_model = None,max_ind = None,to_index = None,inputs = None,outputs = None,name = None):
        self.isinit = False
        self.index2vec = {}
        self.lookup_vector = lambda x: self.index2vec[x] if x in self.index2vec else np.zeros((self.w2v_model.vector_size,))
        if outputs != None:
            self.isinit = True
            Model.__init__(self,inputs,outputs,name = name)
            return
        self.w2v_model = w2v_model
        self.max_ind = max_ind
        self.init(to_index)
    def init(self,to_index):
        self.to_index = to_index
        max_ind = self.max_ind
        w2v_model = self.w2v_model
        embedding_matrix = np.zeros((max_ind + 1, w2v_model.vector_size))

        for i,key in enumerate(["__UNK__","__PAD__"]):
            embedding_matrix[to_index[key]] = np.zeros((self.w2v_model.vector_size,))
            #embedding_matrix[to_index[key]][i] = 1
        
        vocab_list = [(word, w2v_model.wv[word]) for word, _ in w2v_model.wv.vocab.items()]
        for word in to_index:
            try:
                vec = w2v_model.wv[word]
            except KeyError:
                continue
            embedding_matrix[to_index[word]] = vec
            self.index2vec[to_index[word]] = vec

        embeddingLayer = Embedding(input_dim=embedding_matrix.shape[0],
                            output_dim=embedding_matrix.shape[1],
                            weights=[embedding_matrix],
                            trainable=False)

        self.encodeLayer = [Input(shape = (None,))]
        self.decodeLayer = [Input(shape = (None,))]
        self.encodeLayer.append(embeddingLayer(self.encodeLayer[-1]))
        self.decodeLayer.append(embeddingLayer(self.decodeLayer[-1]))
    def add_RNN(self,encodeLayer,decodeLayer):
        out = encodeLayer(self.encodeLayer[-1])
        encodeOutput, state = out[0],out[1:]
        decodeOutput = decodeLayer(self.decodeLayer[-1],initial_state = state)[0]
        self.encodeLayer.append(encodeOutput)
        self.decodeLayer.append(decodeOutput)
    def addBoth(self,layer1,layer2):
        encodeOutput = layer1(self.encodeLayer[-1])
        decodeOutput = layer2(self.decodeLayer[-1])
        self.encodeLayer.append(encodeOutput)
        self.decodeLayer.append(decodeOutput)
    def add(self,layer):
        decodeOutput = layer(self.decodeLayer[-1])
        self.decodeLayer.append(decodeOutput)
    def compile(self,*args,**kargs):
        if not self.isinit:
            Model.__init__(self,[self.encodeLayer[0],self.decodeLayer[0]],self.decodeLayer[-1])
        Model.compile(self,*args,**kargs)
    def fit(self,x,y,epochs,batch_size = 32,model_name = None,prefix = ''):
        """
        model input:
        [input1,input2]
        -input1: encoder input (None,sequence_length)
        -input2: decoder input (None,sequence_length)
        -model output: embedding vector (None,sequence_length,max_ind)
        """
        to_index = self.to_index
        all_onehot = True
        try:
            bos = to_index["__BOS__"]*np.ones((x.shape[0],1))
            eos = to_index["__EOS__"]*np.ones((x.shape[0],1))
            y_all = np.concatenate([bos,y,eos],axis = 1)
            target_all = y_all[:,1:]
            target_all = to_categorical(target_all,num_classes = self.max_ind)
            y_all = y_all[:,:-1]
        except MemoryError:
            y_all = y
            all_onehot = False

        batch_num = x.shape[0] // batch_size + int(x.shape[0]%batch_size != 0)
        for epoch in range(1,epochs+1):
            description = "%sEpoch %d/%d"%(prefix,epoch,epochs)
            bar = tqdm.tqdm(range(batch_num),ncols = 110)
            bar.set_description(description)
            for batch in bar:
                bar.refresh()
                x_batch = x[batch*batch_size:(batch+1)*(batch_size)]
                y_batch = y_all[batch*batch_size:(batch+1)*(batch_size)]
                if all_onehot:
                    target = target_all[batch*batch_size:(batch+1)*(batch_size)]
                else:
                    bos = to_index["__BOS__"]*np.ones((x_batch.shape[0],1))
                    eos = to_index["__EOS__"]*np.ones((x_batch.shape[0],1))
                    y_batch = np.concatenate([bos,y_batch,eos],axis = 1)
                    target = y_batch[:,1:]
                    target = to_categorical(target,num_classes = self.max_ind)
                    y_batch = y_batch[:,:-1]
                result = self.train_on_batch([x_batch, y_batch],target)
                loss,acc = ["%.3f"%i for i in result]
                postfix = {'loss':loss,'acc':acc}
                bar.set_postfix(postfix)
            bar.close()
            if model_name != None:
                model.save(model_name)
    def predict(self,x,maxlen = 10,batch_size = 128):
        to_index = self.to_index
        batch_num = x.shape[0] // batch_size + int(x.shape[0]%batch_size != 0)
        y = np.ones((x.shape[0],maxlen))*to_index["__BOS__"]
        print("Predicting...")
        for batch in tqdm.tqdm(range(batch_num),ncols = 75):
            x_batch = x[batch*batch_size:(batch+1)*(batch_size)]
            y_batch = y[batch*batch_size:(batch+1)*(batch_size)]

            for word_index in range(maxlen):
                if word_index == 0:
                    output = Model.predict(self,[x_batch,y_batch])[:,:,4:].argmax(axis = 2) + 4
                else:
                    output = Model.predict(self,[x_batch,y_batch])[:,:,1:].argmax(axis = 2) + 1
                y_batch[:,word_index+1:] = output[:,word_index:-1]
                if word_index == maxlen-1:
                    y_batch[:,:-1] = y_batch[:,1:]
                    y_batch[:,-1] = output[:,-1]
            y[batch*batch_size:(batch+1)*(batch_size)] = y_batch
        return y
    def super_fit(self,x,y,epochs,epoch_per_batch,batch_size = 32,model_name = None):
        batch_num = x.shape[0] // batch_size + int(x.shape[0]%batch_size != 0)
        for epoch in range(1,epochs+1):
            for batch in range(batch_num):
                x_batch = x[batch*batch_size:(batch+1)*(batch_size)]
                y_batch = y[batch*batch_size:(batch+1)*(batch_size)]
                self.fit(x_batch,y_batch,epoch_per_batch,model_name = model_name,prefix = 'Super %d/%d %d/%d, '%(epoch,epochs,batch+1,batch_num))
            



if __name__ == '__main__':
    if os.path.isdir("training_data"):
        for i in ["x_train","y_train","to_word","to_index","max_ind"]:
            with open("%s/%s.pkl"%("training_data",i),"rb") as file1:
                exec("%s = pickle.load(file1)"%i)
    else:
        os.makedirs("training_data", exist_ok=True)
        data = preprocess.TextDataset(filename = 'mlds_hw2_2_data/clr_conversation.txt',
            embedding_dict = 'Embedding.pkl',threshold = 150,max_len = (10,10))
        x_train,y_train = data.x,data.y
        to_word = data.to_word #index2word
        to_index = data.to_index #word2index
        max_ind = data.num_class
        for i in ["x_train","y_train","to_word","to_index","max_ind"]:
            with open("%s/%s.pkl"%("training_data",i),"wb") as file1:
                pickle.dump(eval(i),file1)

    print(x_train[0])
    print(x_train.shape,y_train.shape)
    print(' '.join([to_word[i] for i in x_train[1]]))
    print(' '.join([to_word[i] for i in y_train[1]]))
    print("%d different words founded in the dataset."%max_ind)
    w2v_model = Word2Vec.load("word2vec/word2vec.model")
    

    if os.path.isfile("model3.hdf5"):
        model = load_model("model3.hdf5",custom_objects={'chineseChatbot': chineseChatbot})
        model.w2v_model = w2v_model
        model.max_ind = max_ind
        model.init(to_index)
    else:
        model = chineseChatbot(w2v_model = w2v_model,max_ind = max_ind,to_index = to_index)

        encodeLayer = CuDNNGRU(128,return_state = True,return_sequences = True)
        decodeLayer = CuDNNGRU(128,return_state = True,return_sequences = True)
        model.add_RNN(encodeLayer,decodeLayer)

        #encodeLayer = BatchNormalization()
        #decodeLayer = BatchNormalization()
        #model.addBoth(encodeLayer,decodeLayer)        

        #encodeLayer = Dropout(0.9)
        #decodeLayer = Dropout(0.9)
        #model.addBoth(encodeLayer,decodeLayer)

        model.add(Dense(3000))
        model.add(LeakyReLU(alpha=0.3))
        model.add(BatchNormalization())
        model.add(Dense(max_ind,activation = 'softmax'))
        
    opt = keras.optimizers.Adam(lr=0.001,clipnorm=1)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['acc'])
    model.summary()
    #x_train = x_train[:1024]
    #y_train = y_train[:1024]
    model.super_fit(x_train, y_train,epochs=100,epoch_per_batch = 20,batch_size = 128,model_name = "model3.hdf5")
