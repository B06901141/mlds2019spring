import pickle
from os import path

import numpy as np
import torch
import tqdm
from torch.utils.data import Dataset

import reader


def preprocess(data_file, embedding_file,  embedding_dim=300, ptoken="+++$+++"):
    embedding_dict = reader.load_vectors(embedding_file)
    embedding_dict["__UNK__"] = np.zeros(embedding_dim)

    print("Reading file... ", end='')
    with open(data_file, encoding='utf-8') as f:
        text = f.read()
    print("Done.")

    paragraphes = text.split(ptoken)
    progbar = tqdm.tqdm(total=len(paragraphes))
    existing_keys = {}
    for parag in paragraphes:
        progbar.update(1)
        lines = parag.split('\n')
        for i in range(len(lines)):
            for k in lines[i].split():
                existing_keys[k] = embedding_dict[k] if k in embedding_dict.keys(
                ) else embedding_dict["__UNK__"]
    progbar.close()
    print("Dict size: {}".format(len(existing_keys)))
    pickle.dump(obj=existing_keys, file=open("Embedding.pkl", 'wb'))


TextDataset = reader.TextDataset

if __name__ == "__main__":
    preprocess(path.join("mlds_hw2_2_data",
                         "clr_conversation.txt"), "./cc.zh.300.vec")
