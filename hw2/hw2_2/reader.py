import os
import re
import subprocess
import pickle

import numpy as np
import tqdm
from torch.utils.data import DataLoader, Dataset

import preprocess

def call(string):
    subprocess.call(string.split())


def look_up(to_index, word):
    return to_index.get(word,to_index["__UNK__"])


def load_vectors(fname):
    print("Loading word vectors... ")
    progbar = tqdm.tqdm(total=len(open(fname, 'r', encoding='utf-8',
                                  newline='\n', errors='ignore').readlines()),ncols = 80)
    fin = open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
    n, d = map(int, fin.readline().split())
    data = {}
    for line in fin:
        progbar.update(1)
        tokens = line.rstrip().split(' ')
        data[tokens[0]] = map(float, tokens[1:])
    progbar.close()
    return data


class TextDataset(Dataset):
    def __init__(self, filename, embedding_dict, embedding_dim=300, ptoken="+++$+++", threshold = 3,max_len = (10,10)):
        super().__init__()

        print("Reading file... ", end='')
        with open(filename, encoding='utf-8') as f:
            text = f.read()
        print("Done.")

        print("Loading embedding dicts...")
        with open(embedding_dict, 'rb') as file1:
            embedding_dict = pickle.load(file1)

        print("Loading embedding matrices...")
        progbar = tqdm.tqdm(total=len(embedding_dict),ncols = 80)
        for key in embedding_dict.keys():
            progbar.update(1)
            embedding_dict[key] = np.array([i for i in embedding_dict[key]],dtype = np.float16)
        progbar.close()
        embedding_dict["__UNK__"] = np.zeros(embedding_dim)

        to_word = [key for key in embedding_dict.keys()]
        to_index = dict([(key, i) for i, key in enumerate(to_word)])

        print("Reading chinese dataset...")
        paragraphes = text.split(ptoken)
        progbar = tqdm.tqdm(total=len(paragraphes),ncols = 80)
        training_pairs = []
        counter = {}
        for parag in paragraphes:
            progbar.update(1)
            lines = parag.split('\n')
            this = [look_up(to_index, key) for key in lines[0].split()]
            for i in range(1, len(lines)):
                other = [look_up(to_index, key)
                         for key in lines[i].split()]
                for j in other:
                    if j not in counter:
                        counter[j] = 1
                    else:
                        counter[j] += 1
                training_pairs.append([this, other])
                this = other
        progbar.close()

        print("Regenerating index...",end = '',flush = True)
        to_index2 = dict(to_index)
        to_index = {}
        to_index["__UNK__"] = 0
        to_index["__BOS__"] = 1
        to_index["__EOS__"] = 2
        to_index["__PAD__"] = 3
        to_index["的啦"] = 4
        now = 5
        for i in to_index2:
            try:
                if counter[to_index2[i]] >= threshold:
                    to_index[i] = now
                    now += 1
            except KeyError:
                pass
        print("Done")

        print("Updating training pairs... ")
        for i in tqdm.tqdm(range(len(training_pairs)),ncols = 80):
            #for j in range(len(training_pairs[i])):
            #training_pairs[i][0] = np.array(
            #    [embedding_dict[to_word[ind]] for ind in training_pairs[i][0]],dtype = np.float16)
            training_pairs[i][0] = [look_up(to_index,to_word[ind]) for ind in training_pairs[i][0]]
            training_pairs[i][1] = [look_up(to_index,to_word[j]) for j in training_pairs[i][1]]

        to_word = {i:j for j,i in to_index.items()}

        progbar.close()
        self.to_word = to_word
        self.to_index = to_index
        self.num_class = now

        print("Spliting x,y...")
        self.x = []
        self.y = []
        for i in tqdm.tqdm(training_pairs,ncols = 80):
            if len(i[1]) != 0 and len(i[0]) != 0:
                pad_num = max_len[0] - len(i[0])
                if pad_num <= 0:
                    self.x.append([look_up(to_index,"__BOS__")] + i[0][-max_len[0]+1:])
                else:
                    self.x.append([look_up(to_index,"__PAD__")]*(pad_num-1) + [look_up(to_index,"__BOS__")] + i[0])
                pad_num = max_len[1] - len(i[1])
                if pad_num <= 0:
                    self.y.append(i[1][-max_len[1]+1:] + [look_up(to_index,"的啦")])
                else:
                    self.y.append(i[1] + [look_up(to_index,"的啦")] + [look_up(to_index,"__EOS__")]*(pad_num-1))
        self.x = np.array(self.x,dtype = np.uint8)
        self.y = np.array(self.y,dtype = np.uint8)

    def __len__(self):
        return len(self.training_pairs)

    def __getitem__(self, index):
        return self.training_pairs[index]

def TextDataLoader(filename, batch_size, shuffle=True, ptoken="+++$+++"):
    return DataLoader(TextDataset(filename, ptoken=ptoken),
                      batch_size=batch_size, shuffle=shuffle)
if __name__ == "__main__":
    # call("wget https://github.com/facebookresearch/fastText/archive/v0.2.0.zip")
    # call("unzip v0.2.0.zip")
    # call("cd fastText-0.2.0")
    # call("make")
    # call("./fasttext")
    if not os.path.exists('cc.zh.300.vec'):
        if not os.path.exists('cc.zh.300.vec.gz'):
            call("wget https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.zh.300.vec.gz")
        call("gunzip cc.zh.300.vec.gz")
    load_vectors("./cc.zh.300.vec")
