import numpy as np
import os
import pickle
import tqdm

from gensim.models.word2vec import Word2Vec
import preprocess
import reader
from model import *
from toSentence import *
from beam_search import *

if __name__ == '__main__':
    if os.path.isdir("training_data"):
        for i in ["x_train","y_train","to_word","to_index","max_ind"]:
            with open("%s/%s.pkl"%("training_data",i),"rb") as file1:
                exec("%s = pickle.load(file1)"%i)
    else:
        os.makedirs("training_data", exist_ok=True)
        data = preprocess.TextDataset(filename = 'mlds_hw2_2_data/clr_conversation.txt',
            embedding_dict = 'Embedding.pkl',threshold = 3,max_len = (10,10))
        x_train,y_train = data.x,data.y
        to_word = data.to_word #index2word
        to_index = data.to_index #word2index
        max_ind = data.num_class
        for i in ["x_train","y_train","to_word","to_index","max_ind"]:
            with open("%s/%s.pkl"%("training_data",i),"wb") as file1:
                pickle.dump(eval(i),file1)

    print(x_train[0])
    print(x_train.shape,y_train.shape)
    print(' '.join([to_word[i] for i in x_train[0]]))
    print(' '.join([to_word[i] for i in y_train[0]]))
    print("%d different words founded in the dataset."%max_ind)

    w2v_model = Word2Vec.load("word2vec/word2vec.model")

    model = load_model("model2.hdf5",custom_objects={'chineseChatbot': chineseChatbot})
    model.w2v_model = w2v_model
    model.max_ind = max_ind
    model.init(to_index)

    model.summary()
    xtest = get_xtest(filename = 'mlds_hw2_2_data/test_input.txt', padding_size = 10, to_index = to_index)
    output = model.predict(xtest,maxlen = 10)
    #np.save('output', output)
    sentences = array_to_sentence(output, to_word)
    sentences_to_file('output/output.txt', sentences)

