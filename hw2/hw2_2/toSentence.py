import sys
import numpy as np

def array_to_sentence(array, dictionary):
	output = []
	for sentence in array:
		tmp = ''
		for code in sentence:
			word = dictionary.get(code,"__UNK__")
			if word != '__EOS__':
				tmp += word + ' '
			else:
				break
		output.append(tmp + '__EOS__')

	return output

def sentences_to_file(filename, sentences):
	sentences = '\n'.join(sentences)
	with open(filename, 'w') as f:
		f.write(sentences + '\n')

if __name__ == '__main__':
	_, arrayname, dictname, filename = sys.argv
	array = np.load(arrayname)
	with open(dictname, 'r', encoding='utf8')as f:
		dictionary_list = [x.split(' ') for x in f.read().split('\n')][:-1]
	dictionary = {int(x[0]):x[1] for x in dictionary_list}

	sentences = array_to_sentence(array, dictionary)
	sentences_to_file(filename, sentences)

	