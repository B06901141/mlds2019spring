from gensim.models.word2vec import Word2Vec
import pickle
import numpy as np
# from keras.utils import to_categorical

class Path:
    def __init__(self, word = '__BOS__', score = 0):
        self.path = [word]
        self.score = score

    def last_word(self):
        if len(self.path) == 0:
            return '__BOS__'
        return self.path[-1]

    def concat(self, word, score):
        p = Path()
        p.path = self.path[:]
        p.path.append(word)
        p.score = self.score + score
        return p

class Wrapdata:
    def __init__(self, i, w, sc):
        self.idx = i
        self.word = w
        self.score = sc

    def __lt__(self, other):
        return self.score < other.score

def beam_search(model, vectors, beam_size=20, query_size=100):
    to_word = pickle.load(open('training_data/to_word.pkl', 'rb'))
    to_index = pickle.load(open('training_data/to_index.pkl', 'rb'))
    idx = np.flip(np.argsort(vectors[0]))[:beam_size]
    paths = [Path(to_word[i]) for i in idx]
    # print(paths[0].path)

    for wv in vectors[1:]:
        idx = np.flip(np.argsort(wv))[:query_size]
        # print(to_word[idx[0]])
        # print(idx)
        words = [to_word[i] for i in idx]
        last_words = [p.last_word() for p in paths]
        new_paths = []
        for i, worda in enumerate(last_words):
            for j, wordb in enumerate(words):
                scoreL = model.score([worda, wordb])
                score = scoreL[0] - scoreL[1]
                new_paths.append(Wrapdata(i, wordb, score))
                # print("%.4f %4s %7s %.4f"%(paths[i].score, worda, wordb, score), end='                 \r')
        new_paths.sort()
        # actions = [:beam_size]
        paths = [paths[d.idx].concat(d.word, d.score) for d in new_paths[:beam_size]]
    return paths[0].path

def get_xtest(filename, padding_size, to_index):
    x_test = []
    with open(filename, 'r', encoding='utf-8') as fin:
        for line in fin:
            words = line.rstrip('\n').split(' ')
            idx = []
            for w in words:
                if w in to_index.keys():
                    idx.append(to_index[w])
                else:
                    idx.append(to_index['__UNK__'])
            # [to_index[w] if w in to_index else to_index['__UNK__'] ]
            if len(idx) < padding_size:
                bos =  [to_index['__PAD__']] * (padding_size - len(idx)-1) + [to_index['__BOS__']]
                bos.extend(idx)
                #print(bos)
            else:
                bos = [to_index['__BOS__']] + idx[-padding_size+1:]
                #print(bos)
            x_test.append(bos)
    x_test = np.array(x_test,dtype = np.float64)
    return x_test

if __name__ == '__main__':

    import time
    embedding_model = Word2Vec.load('word2vec/word2vec.model')
    #predict_model = Word2Vec.load('w2v_pad.model')
    to_index = pickle.load(open('training_data/to_index.pkl', 'rb'))
    '''
    test_input = ['我', '和', '他', '近在咫尺', ',', '他', '卻', '跑', '了']
    max_ind = pickle.load(open('training_data/max_ind.pkl', 'rb'))
    vectors = [np.zeros(max_ind) for _ in range(len(test_input))]
    for wv in vectors:
        for i in range(len(wv)):
            r = np.random.random() / 10000
            wv[i] = r
    for i, w in enumerate(test_input):
        vectors[i][to_index[w]] = 1

    t = time.time()
    # res = embedding_model.most_similar(test_input[0])
    pred = beam_search(predict_model, vectors, beam_size=10, query_size=10)
    # print(res)
    print(time.time() - t)
    print(pred)
    '''
    print(type(to_index))
    print(to_index['__UNK__'])
    x_test = get_xtest('mlds_hw2_2_data/test_input.txt', 10, to_index)
    print(x_test.shape)