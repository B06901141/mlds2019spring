from gensim.models.word2vec import Word2Vec
import tqdm
import os

def readTrain(fileName,ptoken="+++$+++"):
    with open(fileName,'r') as file1:
        s = file1.read().split(ptoken)[:-1]
    result = []
    for i in s:
        result.extend(i.split('\n'))
    result = [i.split(" ") for i in result]
    print("Reading training data...")
    for i in tqdm.tqdm(result,ncols = 80):
        while '' in i:
            i.remove('')
    return result
def readTest(fileName):
    with open(fileName,'r') as file1:
        s = file1.read().split("\n")[:-1]
    result = [i.split(" ") for i in s]
    result = [i.split(" ") for i in s]
    for i in tqdm.tqdm(result,ncols = 80):
        while '' in i:
            i.remove('')
    return result
def parseSentence(sentences):
    return [["__BOS__"] + i + ["的啦"] + ["__EOS__"] for i in sentences]

if __name__ == '__main__':
    if os.path.isfile("word2vec/word2vec.model"):
        model = Word2Vec.load("word2vec/word2vec.model")
    else:
        os.makedirs("word2vec",exist_ok = True)
        sentences = readTrain("mlds_hw2_2_data/clr_conversation.txt")
        sentences.extend(readTest("mlds_hw2_2_data/test_input.txt"))
        sentences = parseSentence(sentences)
        print("Training word2vec...",end = '',flush = True)
        model = Word2Vec(sentences, size=100, window=5, min_count=1)
        model.save("word2vec/word2vec.model")
        print("Done")

    for key in model.wv.similar_by_word('美國', topn =10):
        print(key)