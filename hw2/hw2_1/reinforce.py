import json
import sys
from argparse import ArgumentParser
from os import makedirs
from os.path import join

import numpy as np
import torch
from torch import nn, optim
from torch.distributions import Categorical
from torch.nn import functional as F
from torch.utils.data import DataLoader, Dataset

from bleu_eval import BLEU
from loader import EfficientVideoCaptionDataset, Lang
from main import AttnDecoderRNN, EncoderRNN, one_hot


class Policy(nn.Module):
    def __init__(self, num_embeddings, hidden_size=300, timesteps=80):
        super().__init__()
        self.enc = EncoderRNN(hidden_size=hidden_size)
        self.dec = AttnDecoderRNN(num_embeddings, hidden_size=hidden_size)
        self.expected = nn.Sequential(
            nn.ReLU(),
            nn.Linear(in_features=num_embeddings,
                      out_features=num_embeddings // 2),
            nn.ReLU(inplace=True),
            nn.Linear(in_features=num_embeddings // 2, out_features=1)
        )

    def forward(self, input):
        output = self.dec(self.enc(input))
        return output, self.expected(output)


def get_labels(mode='train', path='MLDS_hw2_1_data'):

    path = join(path, mode + "ing_label.json")
    with open(path, "r") as file:
        _label = json.load(fp=file)

    return _label


def get_labels_from_id(id, label):
    for i in range(len(label)):
        if label[i]['id'].rstrip() == id:
            return label[i]['caption']
    return id


class FromID:
    def __init__(self, label):
        self.label = label

    def __call__(self, id):
        return get_labels_from_id(id, self.label)


def blue(s, t):
    if len(s) == 0:
        print(blue, ValueError)
        return 0
    return BLEU(s, t, True)


def softmax(tensor):
    return F.softmax(tensor.view(1, -1), -1)


def pre_train(epochs,
              hidden_size,
              threshold,
              lr,
              weight_dir,
              batch_size,
              restarts,
              device='cpu'):

    if device == 'cuda':
        if not torch.cuda.is_available():
            device = 'cpu'

    namelist = []

    restart, repochs = restarts

    dataset = EfficientVideoCaptionDataset(
        mode='train', device=device,
        _lang=Lang(threshold), vid_name_list=namelist
    )
    dataloader = DataLoader(dataset, batch_size=batch_size,
                            shuffle=True, drop_last=True)

    lang = dataset.get_lang()

    np.save(file=join(weight_dir, 'word_index.npy'), arr=lang.word_index)
    np.save(file=join(weight_dir, 'index_word.npy'), arr=lang.index_word)

    model = Policy(num_embeddings=lang.size).to(device)

    print(model)

    encoder = model.enc
    decoder = model.dec
    Qfunc = model.expected

    if not restart:
        optimizer = [
            optim.RMSprop(params=encoder.parameters(), lr=lr),
            optim.RMSprop(params=decoder.parameters(), lr=lr),
            optim.RMSprop(params=Qfunc.parameters(), lr=lr)
        ]

    loss_fn = nn.CrossEntropyLoss()

    avg_loss = []
    for epoch in range(1, 1 + epochs):
        print('pretrain epoch: {}/{}'.format(epoch, epochs))
        if restart:
            if epoch-1 % repochs == 0:
                optimizer = [
                    optim.RMSprop(params=encoder.parameters(), lr=lr),
                    optim.RMSprop(params=decoder.parameters(), lr=lr),
                    optim.RMSprop(params=Qfunc.parameters(), lr=lr)
                ]
        for vid, label in dataloader:
            loss = torch.tensor(0., device=device)

            states = torch.zeros(1, batch_size, hidden_size, device=device)

            enc_output, states = encoder(vid, states)
            # Teacher forcing
            for idx in range(int(label.shape[-1]) - 1):
                (out, states) = decoder(one_hot(label[:, idx],
                                                lang.size,
                                                device=device),
                                        states, enc_output, batch_size=batch_size)
                loss += loss_fn(out, label[:, idx + 1])

            for rms in optimizer:
                rms.zero_grad()
            loss.backward()
            for rms in optimizer:
                rms.step()
            # print(loss)
            avg_loss.append(loss.item())
        print('avg loss: {}'.format(sum(avg_loss) / len(avg_loss)))
        torch.save(obj=model.state_dict(),
                   f=join(weight_dir, 'pretrain_{:03d}.pth'.format(epoch)))
    return model, optimizer, dataset, namelist, loss_fn, lang


def train_one_epoch(trials,
                    model,
                    optimizer,
                    dataset,
                    namelist,
                    loss_fn,
                    lang,
                    hidden_size,
                    fromid,
                    gamma,
                    zeta,
                    sos,
                    endlist,
                    device='cpu'):
    model.to(device)
    encoder = model.enc
    decoder = model.dec
    expectation = model.expected
    for _ in range(trials):
        for (vid, _), name in zip(dataset, namelist):
            vid = vid.unsqueeze(0)
            # create batch size = 1
            sentence = []
            value_list = []
            reward_list = []
            log_probability = []

            loss = torch.tensor(0., device=device)
            states = torch.zeros(1, 1, hidden_size, device=device)

            enc_output, states = encoder(vid, states)

            word_output = torch.tensor([sos], device=device)

            while not word_output.item() in endlist:
                (dec_out, states) = decoder(one_hot(word_output,
                                                    max=lang.size,
                                                    device=device),
                                            states,
                                            enc_output)
                val = expectation(dec_out)
                value_list.append(val.squeeze())
                dist = Categorical(softmax(dec_out))
                sample = dist.sample()
                # print(sample)
                sentence.append(lang.index_word[sample.squeeze().item()])
                b = blue(' '.join(sentence), fromid(name))
                reward_list.append(b - val.squeeze().item())

                log_probability.append(dist.log_prob(sample).squeeze())

                if len(sentence) >= 35:
                    break
            next_state_reward = 0
            for val, reward, log_prob in zip(*map(
                reversed, (value_list, reward_list, log_probability)
            )):
                reward += gamma * next_state_reward
                next_state_reward = reward
                loss += -log_prob * reward
                loss += F.smooth_l1_loss(val,
                                         torch.tensor(reward, device=device))

            for rms in optimizer:
                rms.zero_grad()
            loss.backward()
            for param in decoder.parameters():
                param.grad *= zeta
            for rms in optimizer:
                rms.step()

    return model, optimizer, dataset, namelist, loss_fn, lang, fromid


def predict(hidden_size,
            model=None,
            on_epoch=300,
            load_pretrain=False,
            interval=1,
            weight_dir='weight_dir',
            file_name='output.txt',
            device='cpu'):
    model.to(device)
    name_list = []
    test = EfficientVideoCaptionDataset(
        mode='test', vid_name_list=name_list)

    on_epoch = on_epoch // interval * interval

    if not model:
        if load_pretrain:
            model = torch.load(f='pretrain_{:03d}.pth'.format(on_epoch),
                               map_location=device)
        else:
            model = torch.load(f='training_{:03d}.pth'.format(on_epoch),
                               map_location=device)

    print(model)
    enc = model.enc
    dec = model.dec

    index_word = np.load(join(weight_dir, 'index_word.npy')).item()
    word_index = np.load(join(weight_dir, 'word_index.npy')).item()

    sos = word_index['__SOS__']
    pad = word_index['__PAD__']
    eos = word_index['__EOS__']

    end_list = [pad, eos]

    all_sentences = []

    vocab_size = len([k for k in index_word.keys()])

    for (vid, _), name in zip(test, name_list):
        vid = vid.unsqueeze(0)
        # states = (torch.zeros(1, 1, hidden_size, device='cpu'),
        #           torch.zeros(1, 1, hidden_size, device='cpu'))
        states = (torch.zeros(1, 1, hidden_size, device=device))

        enc_out, states = enc(vid, states)

        word_output = torch.tensor([sos], device=device)

        sentence = []
        while not word_output.item() in end_list:
            dec_out, states = dec(one_hot(word_output,
                                          max=vocab_size,
                                          device=device),
                                  states,
                                  enc_out)
            word_output = torch.argmax(dec_out)
            index = word_output.item()
            sentence.append(index_word[index])
            word_output = torch.tensor([word_output])
            if len(sentence) >= 35:
                print(' '.join(sentence))
                break
        sentence = ' '.join(sentence[:-1])
        sentence = ','.join([name, sentence])
        all_sentences.append(sentence)
    all_sentences = '\n'.join(all_sentences)

    with open(file_name, 'w+') as file:
        file.write(all_sentences)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('--epochs', type=int, required=True)
    parser.add_argument('--trials', type=int, default=200)
    parser.add_argument('--pretrain', type=int, required=True)
    parser.add_argument('--threshold', type=int, default=5)
    parser.add_argument('--hidden', type=int, default=300)
    parser.add_argument('--device', type=str, default='cpu')
    parser.add_argument('--lr', type=float, default=1e-3)
    parser.add_argument('--gamma', type=float, default=.9)
    parser.add_argument('--zeta', type=float, default=.01)
    parser.add_argument('--weight_dir', type=str, default='weight_dir')
    parser.add_argument('--batch', type=int, default=50)
    parser.add_argument('--restart', action='store_true')
    parser.add_argument('--repochs', type=int, default=50)
    args = parser.parse_args()

    fromid = FromID(get_labels())

    makedirs(name=args.weight_dir, exist_ok=True)

    (model, optimizer, dataset,
     namelist, loss_fn, lang) = pre_train(args.pretrain,
                                          args.hidden,
                                          args.threshold,
                                          args.lr,
                                          args.weight_dir,
                                          args.batch,
                                          (args.restart, args.repochs),
                                          args.device)
    predict(hidden_size=args.hidden, model=model,
            on_epoch=args.epochs, weight_dir=args.weight_dir, file_name='pretrain.txt')
    for params in model.enc.parameters():
        params.requires_grad = False
    endlist = [lang.word_index['__EOS__'], lang.word_index['__PAD__']]
    for epoch in range(1, 1 + args.epochs // 2):
        if args.restart:
            if epoch-1 % args.repochs == 0:
                optimizer = [
                    optim.RMSprop(params=model.enc.parameters(),
                                  lr=args.lr),
                    optim.RMSprop(params=model.dec.parameters(),
                                  lr=args.lr),
                    optim.RMSprop(params=model.expected.parameters(),
                                  lr=args.lr)
                ]
        print('epoch {}/{}'.format(epoch, args.epochs))
        zeta = 0. if epoch < 1 + args.epochs // 2 else args.zeta
        train_one_epoch(args.trials,
                        model,
                        optimizer,
                        dataset,
                        namelist,
                        loss_fn,
                        lang,
                        args.hidden,
                        fromid,
                        args.gamma,
                        zeta,
                        lang.word_index['__SOS__'],
                        endlist,
                        args.device)
        torch.save(obj=model.state_dict(),
                   f=join(args.weight_dir, 'training_{:03d}.pth'.format(epoch)))

    predict(hidden_size=args.hidden, model=model,
            on_epoch=args.epochs, weight_dir=args.weight_dir, file_name='rl.txt')
