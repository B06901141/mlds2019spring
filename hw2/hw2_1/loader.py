import json
import os
import sys

import numpy as np
import torch
from torch.utils.data import Dataset


class Lang:
    '''@Interface:
    w2i: mapping from word to index
    size: returns the size of the vocab
    add_word: adding word to the dictionary
    word_index: word -> index in dictionary form
    index_word: index -> word in dictionary form
    test: returns self, set untrainable
    build: builds
    '''

    def __init__(self, threshold):
        self._threshold = threshold
        self.word_n_count = {}
        self.keys = []
        self.reserved = ['__SOS__', '__EOS__', '__PAD__', '__UNK__']
        self.trainable = True
        self.built = False
        self.dictionary = None

    def w2i(self, word):
        try:
            return self.word_index[word]
        except KeyError:
            return self.word_index['__UNK__']

    def add_word(self, word):
        if self.trainable:
            if word in self.keys:
                self.word_n_count[word] += 1
            else:
                self.keys.append(word)
                self.word_n_count[word] = 1

    @property
    def word_index(self):
        if self.dictionary is None:
            raise ValueError('None')
        try:
            return self._word_index
        except:
            self._word_index = dict(
                [(word, index) for index, word in enumerate(self.dictionary)])
            return self._word_index

    @property
    def index_word(self):
        if self.dictionary is None:
            raise ValueError('None')
        try:
            return self._index_word
        except:
            self._index_word = dict(
                [(index, word) for index, word in enumerate(self.dictionary)])
            return self._index_word

    @property
    def size(self):
        return len([val for val in self.word_n_count.values() if val >= self._threshold]) + len(self.reserved)

    def test(self):
        self.trainable = False
        return self

    def build(self):
        if self.built:
            return
        if not self.trainable:
            return
        self.dictionary = [ch for ch in self.reserved]

        self.dictionary.extend(
            [key for key in self.keys if self.word_n_count[key] >= self._threshold])

        self.built = True
        self.trainable = False


class EfficientVideoCaptionDataset(Dataset):
    def __init__(self, mode, path=None, device="cpu", _lang=None, vid_name_list=None):
        """__init__ of the class

        Arguments:
            mode {[str]} -- [can only be "train" or "test"]

        Keyword Arguments:
            path {[str]} -- [the path to data directory, don't modify it if you do not want to provide your own dataset] (default: {None})
            device {[torch.device]} --  (default: {torch.device("cpu")})
            _lang {[Lang]} -- [language recorder] (default: {Lang()})
        """
        if not _lang:
            print('Using default word counter')
            _lang = Lang(5)

        super().__init__()

        if not path:
            path = "MLDS_hw2_1_data"

        videos = self.__get_videos(mode, path)
        label = self.__get_labels(mode, path)

        self._lang = _lang

        _videos = []
        _labels = []
        # a list of sentences

        for idx in range(len(label)):
            video_name = label[idx]["id"]
            if vid_name_list != None:
                vid_name_list.append(video_name)
            video = videos[video_name]
            _videos.append(video)
            for sentence in label[idx]["caption"]:
                sentence.replace(".", " . ")
                sentence.replace("-", " - ")
                words = sentence.rstrip().split(" ")
                for word in words:
                    self._lang.add_word(word)

        self._lang.build()

        for idx in range(len(label)):
            _indexes = []
            for sentence in label[idx]["caption"]:
                sentence.replace(".", " . ")
                sentence.replace("-", " - ")
                words = sentence.rstrip().split(" ")
                words.insert(0, "__SOS__")
                words.append("__EOS__")
                indexes = [self._lang.w2i(word) for word in words]
                _indexes.append(indexes)
            _labels.append(_indexes)

        _videos = np.array(_videos, dtype=np.float32)
        self._videos = torch.tensor(_videos, device=device)

        maxlen = 25
        largest = 0
        for sentence in _labels:
            largest = max(len(sentence), largest)
        largest = min(largest, maxlen)
        for list in _labels:
            for sentence in list:
                for _ in range(len(sentence) - largest):
                    sentence.pop()
        for list in _labels:
            for sentence in list:
                sentence.extend(
                    [self._lang.w2i("__PAD__")] *
                    (largest - len(sentence))
                )
        # print(self._lang.dictionary)
        # print([self._lang.w2i(i) for i in self._lang.dictionary])
        # print(self._lang.word_index)
        _labels = [np.array(label, dtype=np.long) for label in _labels]
        self._labels = [torch.tensor(label, device=device)
                        for label in _labels]

    def __len__(self):
        """
        Inherited method
        """
        assert len(self._videos) == len(self._labels)
        return len(self._videos)

    def __getitem__(self, idx):
        """
        Inherited method
        """
        label = self._labels[idx]
        random = np.random.randint(len(label))
        return self._videos[idx], self._labels[idx][random]

    def __get_videos(self, mode, path):
        """Returns a dictionary mapping names to videos

        Arguments:
            mode {[str]} -- ["train" or "test"]
            path {[str]} -- [do not modify]

        Returns:
            [dict] -- [names: videos]
        """

        path = os.path.join(path, mode + "ing_data")
        videos = []

        with open(os.path.join(path, "id.txt")) as file:
            data_list = file.read().rstrip().split("\n")

        feat = os.path.join(path, "feat")

        for video in data_list:
            data = np.load(file=os.path.join(feat, video + ".npy"))
            videos.append((video, data))

        return dict(videos)

    def __get_labels(self, mode, path):
        """Returns a bunch of lists of labels

        Arguments:
            mode {[str]} -- ["train" or "test"]
            path {[str]} -- [do not modify]

        Returns:
            [list] -- [list of labels]
        """

        path = os.path.join(path, mode + "ing_label.json")
        with open(path, "r") as file:
            label = json.load(fp=file)

        return label

    def get_lang(self):
        """
        Returns:
            [Lang] -- [language counter]
        """

        return self._lang


if __name__ == "__main__":
    dataset = EfficientVideoCaptionDataset("train", path="../MLDS_hw2_1_data")
    print(len(dataset))
    vid0, cap0 = dataset[0]
    print(vid0.shape)
    print(cap0.shape)
