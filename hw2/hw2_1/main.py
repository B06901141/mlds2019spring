import os
from argparse import ArgumentParser

import ignite
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from ignite.engine import Engine, Events
from ignite.handlers import (EarlyStopping, ModelCheckpoint, TerminateOnNan,
                             Timer)
from ignite.metrics import Accuracy, RunningAverage
from torch.utils.data import DataLoader

try:
    from . import loader
except:
    import loader


class EncoderRNN(nn.Module):
    def __init__(self, input_size=4096, hidden_size=300):
        super(EncoderRNN, self).__init__()
        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size)

    def forward(self, input, states):
        tensor = input.permute(1, 0, 2)
        output, states = self.gru(tensor, states)
        return output, states


class AttnDecoderRNN(nn.Module):
    def __init__(self,
                 num_embeddings,
                 hidden_size=300,
                 timesteps=80):
        embedding_dim = hidden_size
        super(AttnDecoderRNN, self).__init__()
        # self.embedding = nn.Embedding(
        #     num_embeddings=num_embeddings, embedding_dim=embedding_dim
        # )
        self.embedding = nn.Linear(
            in_features=num_embeddings, out_features=embedding_dim)
        self.attn = nn.Linear(in_features=2 * embedding_dim,
                              out_features=timesteps)
        self.combine = nn.Linear(
            in_features=2 * hidden_size, out_features=hidden_size)
        self.gru = nn.GRU(input_size=embedding_dim, hidden_size=hidden_size)
        self.output = nn.Linear(in_features=hidden_size,
                                out_features=num_embeddings)

    def forward(self, input, states, encoder_output, batch_size=1):
        """
        In the attention decoder RNN,
        1. attention is applied to encoder outputs
        2. attention depends on the current state and the current output

        Arguments:
            input {[torch.tensor]} -- [last word]
            states {[tuple]} -- [(h,c) for LSTM or h for GRU]
            encoder_output {[torch.tensor]} -- [encoder output]

        Returns:
            [torch.tensor] -- [current output word]
        """
        # (hidden, ctrl) = states
        embedded = F.relu(self.embedding(input)).view(batch_size, 1, -1)
        # batch, timestep, embedding_dim

        attn = torch.cat((embedded.permute(1, 0, 2), states),
                         dim=-1).permute(1, 0, 2)
        # batch, timestep, embedding_dim*2

        attn = F.relu(self.attn(attn))
        # batch, timestep, timesteps

        encoder_output = encoder_output.permute(1, 0, 2)
        # batch, timesteps, hidden_size

        attn_applied = torch.bmm(attn, encoder_output)
        # batch, timestep, hidden_size

        rnn_input = torch.cat((embedded, attn_applied), dim=-1)
        # batch, timestep, hidden_size*2 (hidden_size = embedding_dim)

        rnn_input = rnn_input.permute(1, 0, 2)

        rnn_input = F.relu(self.combine(rnn_input))
        # timestep, batch, hidden_size

        embedded = F.dropout(embedded, p=0.3)
        tensor, states = self.gru(rnn_input, states)
        # timestep, batch, hidden_size

        tensor = tensor.permute(1, 0, 2).view(batch_size, -1)

        output = self.output(tensor)
        # batch, output_dim

        return output, states


def one_hot(tensor, max, device):
    batch = len(tensor)
    oh = torch.zeros(batch, max, device=device)
    for i in range(batch):
        oh[i, int(tensor[i])] = 1.
    return oh


def ignite_main(epochs,
                hidden_size,
                threshold,
                train,
                device="cpu",
                early=False,
                patience=10,
                enc_lr=1e-2,
                dec_lr=1e-2,
                batch_size=30,
                interval=2):
    '''Run with pytorch-ignite, attention model

    Arguments:
        epochs {`int`} -- number of iterations to run
        device {`str`} -- device to run on
        hidden_size {`int`} -- number of hidden units of rnn
    '''
    if device == "cuda":
        if not torch.cuda.is_available():
            device = "cpu"

    train_dataset = loader.EfficientVideoCaptionDataset(
        mode="train", device=device, _lang=loader.Lang(threshold))
    lang = train_dataset.get_lang()

    test_dataset = loader.EfficientVideoCaptionDataset(
        mode="test", device=device, _lang=lang)

    np.save(file='word_index.npy', arr=lang.word_index)
    np.save(file='index_word.npy', arr=lang.index_word)

    train_loader = DataLoader(train_dataset,
                              batch_size=batch_size,
                              shuffle=True,
                              drop_last=True)
    test_loader = DataLoader(test_dataset,
                             batch_size=batch_size,
                             shuffle=False,
                             drop_last=True)

    encoder = EncoderRNN(hidden_size=hidden_size).to(device)

    optim_enc = optim.RMSprop(encoder.parameters(), lr=enc_lr)

    decoder = AttnDecoderRNN(num_embeddings=lang.size,
                             hidden_size=hidden_size).to(device)

    optim_dec = optim.RMSprop(decoder.parameters(), lr=dec_lr)

    print(encoder)
    print(decoder)

    loss_fn = nn.CrossEntropyLoss()

    def step(engine, batch):
        video, label = batch

        loss = torch.tensor(0., device=device)
        # states = (torch.zeros(1, batch_size, hidden_size, device=device),
        #           torch.zeros(1, batch_size, hidden_size, device=device))
        states = torch.zeros(1, batch_size, hidden_size, device=device)

        enc_output, states = encoder(video, states)
        # Teacher forcing
        for idx in range(int(label.shape[-1]) - 1):
            out, states = decoder(one_hot(
                label[:, idx], lang.size, device=device), states, enc_output, batch_size=batch_size)
            loss += loss_fn(out, label[:, idx + 1])

        optim_enc.zero_grad()
        optim_dec.zero_grad()

        loss.backward()

        optim_enc.step()
        optim_dec.step()

        return {
            "loss": loss.item()
        }

    engine = Engine(step)

    timer = Timer(average=True)
    timer.attach(engine, step=Events.EPOCH_COMPLETED)
    RunningAverage(alpha=.98, output_transform=lambda x: x["loss"]).attach(
        engine, "running_loss")

    # Accuracy(output_transform=lambda o: o['loss']).attach(engine, "acc")

    @engine.on(Events.EPOCH_STARTED)
    def print_epoch(engine):
        print('Epoch: {}'.format(engine.state.epoch))

    if not train:
        @engine.on(Events.ITERATION_STARTED)
        def print_iteration(engine):
            print('Iteration: {}'.format(engine.state.iteration))

        @engine.on(Events.ITERATION_COMPLETED)
        def nameless(engine):
            print('Average loss: {}'.format(
                engine.state.metrics['running_loss']))

    def epoch_completed(engine, timer):
        print('Average loss: {}'.format(engine.state.metrics['running_loss']))
        print('Epoch completed. Took {} seconds'.format(timer.value()))

    engine.add_event_handler(Events.EPOCH_COMPLETED, epoch_completed, timer)

    if early:
        def score_function(engine):
            return -1 * engine.state.metrics["loss"]
        earlystopping = EarlyStopping(
            patience=patience, score_function=score_function, trainer=engine)
        engine.add_event_handler(Events.EPOCH_COMPLETED, earlystopping)

    modelcheckpoint = ModelCheckpoint(dirname='model_weights',
                                      filename_prefix='attention-rnn',
                                      save_interval=interval,
                                      n_saved=10,
                                      create_dir=True,
                                      require_empty=False)
    engine.add_event_handler(Events.EPOCH_COMPLETED,
                             modelcheckpoint,
                             {'dec': decoder, 'enc': encoder})

    # terminateonnan = TerminateOnNan(output_transform=lambda o: o['loss'])
    # engine.add_event_handler(Events.EPOCH_COMPLETED, terminateonnan)
    if train:
        engine.run(train_loader, max_epochs=epochs)
    else:
        engine.run(test_loader, max_epochs=epochs)


def predict(hidden_size, on_epoch=300, interval=2):
    name_list = []
    test = loader.EfficientVideoCaptionDataset(
        mode='test', vid_name_list=name_list)
    tl = DataLoader(test, shuffle=False)

    on_epoch = on_epoch // interval * interval

    dec = torch.load(os.path.join(
        'model_weights', 'attention-rnn_dec_{}.pth'.format(on_epoch)), map_location='cpu')
    print(dec)
    enc = torch.load(os.path.join(
        'model_weights', 'attention-rnn_enc_{}.pth'.format(on_epoch)), map_location='cpu')
    print(enc)

    index_word = np.load('index_word.npy').item()
    word_index = np.load('word_index.npy').item()

    sos = word_index['__SOS__']
    pad = word_index['__PAD__']
    eos = word_index['__EOS__']

    end_list = [pad, eos]

    all_sentences = []

    vocab_size = len([k for k in index_word.keys()])

    for (vid, _), name in zip(tl, name_list):

        # states = (torch.zeros(1, 1, hidden_size, device='cpu'),
        #           torch.zeros(1, 1, hidden_size, device='cpu'))
        states = (torch.zeros(1, 1, hidden_size, device='cpu'))

        enc_out, states = enc(vid, states)

        word_output = torch.tensor([sos], device='cpu')

        sentence = []
        while not word_output.item() in end_list:
            dec_out, states = dec(
                one_hot(word_output, max=vocab_size, device='cpu'), states, enc_out)
            word_output = torch.argmax(dec_out)
            index = word_output.item()
            sentence.append(index_word[index])
            word_output = torch.tensor([word_output])
            if len(sentence) > 35:
                print(' '.join(sentence))
                break
        sentence = ' '.join(sentence[:-1])
        sentence = ','.join([name, sentence])
        all_sentences.append(sentence)
    all_sentences = '\n'.join(all_sentences)

    with open('output.txt', 'w+') as file:
        file.write(all_sentences)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('epochs', type=int,)
    parser.add_argument('--device', type=str, default='cuda')
    parser.add_argument('--threshold', type=int, default=4)
    parser.add_argument('--hidden', type=int, default=300)
    parser.add_argument('--early', type=bool, default=False)
    parser.add_argument('--patience', type=int, default=10)
    parser.add_argument('--test', action='store_true')
    parser.add_argument('--predict', action='store_true')
    parser.add_argument('--batch', type=int, default=30)
    parser.add_argument('--enc_lr', type=float, default=1e-3)
    parser.add_argument('--dec_lr', type=float, default=1e-3)
    parser.add_argument('--interval', type=int, default=2)
    args = parser.parse_args()

    if args.test:
        ignite_main(args.epochs,
                    args.hidden,
                    args.threshold,
                    False,
                    args.device,
                    args.early,
                    args.patience,
                    args.enc_lr,
                    args.dec_lr,
                    args.batch,
                    args.interval)
    else:
        ignite_main(args.epochs,
                    args.hidden,
                    args.threshold,
                    True,
                    args.device,
                    args.early,
                    args.patience,
                    args.enc_lr,
                    args.dec_lr,
                    args.batch,
                    args.interval)
    if args.predict:
        predict(args.hidden, on_epoch=args.epochs, interval=args.interval)
