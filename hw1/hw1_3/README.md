# README

###### tags: `MLDS` `hw1_3`

## subtasks is store in different folders
> [./randomlabel/](./randomlabel/): Can network fit random labels?    
> [./numParam/](./numParam/) : Number of parameters v.s. Generalization   
> [./combine_two_weights/](./combine_two_weights/): Flatness v.s. Generalization - part1   
> [./flatness_batchsize/](./flatness_batchsize/): Flatness v.s. Generalization - part2  
