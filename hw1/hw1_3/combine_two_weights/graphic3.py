import numpy as np
import matplotlib.pyplot as plt
import os

def plot_figure(theta1=1, theta2=2, name = './part3/figure3_1.png'):
	if not os.path.isdir('./part3'):os.mkdir('part3')
	x = np.load('./part3/x_linspace.npy')
	train_perf = np.load('./part3/train_performances('+str(theta1)+', '+str(theta2)+').npy').T
	test_perf = np.load('./part3/test_performances('+str(theta1)+', '+str(theta2)+').npy').T

	train_loss, train_acc = train_perf[0], train_perf[1]
	test_loss, test_acc = test_perf[0], test_perf[1]
	
	fig = plt.figure()
	ax = fig.add_subplot(111)

	lns1 = ax.plot(x, train_loss, 'b', label = 'train')
	lns2 = ax.plot(x, test_loss, 'b--', label = 'test')
	ax2 = ax.twinx()
	lns3 = ax2.plot(x, train_acc, 'r')
	lns4 = ax2.plot(x, test_acc, 'r--')

	lns = lns1 + lns2 + lns3 + lns4
	labs = [l.get_label() for l in lns]
	ax.legend(lns, labs, loc=0)

	ax.grid()
	ax.set_xlabel('alpha')
	ax.set_xlim(x[0], x[-1])
	ax.set_ylabel('cross_entropy', color = 'b')
	ax.tick_params(axis='y', colors='b')
	ax2.set_ylabel('accuracy', color = 'r')
	ax2.tick_params(axis='y', colors='r')
	plt.savefig(name)

if __name__ == '__main__':
	plot_figure(theta1=1, theta2=2, name='./part3/figure3.png')


