from keras.models import Sequential, load_model
from keras.layers import InputLayer, Conv2D, MaxPooling2D, Dropout, Flatten, Dense, Activation, BatchNormalization

def model1(input_shape):#01_epochs_20:acc=70%
	model = Sequential()
	model.add(InputLayer(input_shape))
	model.add(Conv2D(filters = 64, 
					 kernel_size = (8,8),
					 activation = 'relu',
					 padding = 'same'))
	model.add(Dropout(0.1))
	model.add(Conv2D(filters = 64,
					 kernel_size = (6,6),
					 activation = 'relu',
					 padding = 'same'))
	model.add(MaxPooling2D((2,2)))
	model.add(Dropout(0.3))
	model.add(Conv2D(filters = 64,
					 kernel_size = (4,4),
					 activation = 'relu',
					 padding = 'same'))
	model.add(Dropout(0.1))
	model.add(Conv2D(filters = 64, 
					 kernel_size = (3,3),
					 activation = 'relu',
					 padding = 'same'))
	model.add(Dropout(0.1))
	model.add(Conv2D(filters = 32,
					 kernel_size = (3,3),
					 activation = 'relu',
					 padding = 'same'))
	model.add(Dropout(0.1))
	model.add(Conv2D(filters = 16,
					 kernel_size = (3,3),
					 activation = 'relu',
					 padding = 'same'))
	model.add(MaxPooling2D((2,2)))
	model.add(Dropout(0.3))
	model.add(Flatten())
	model.add(Dense(64))
	model.add(Dense(10, activation = 'softmax'))

	return model

def model2(input_shape):
	model = Sequential()
	model.add(InputLayer(input_shape))
	model.add(Conv2D(filters = 64, 
					 kernel_size = (8,8),
					 padding = 'same'))
	model.add(BatchNormalization())
	model.add(Activation('relu'))
	model.add(Dropout(0.15))
	model.add(Conv2D(filters = 64,
					 kernel_size = (6,6),
					 padding = 'same'))
	model.add(BatchNormalization())
	model.add(Activation('relu'))
	model.add(MaxPooling2D((2,2)))
	model.add(Dropout(0.35))
	model.add(Conv2D(filters = 64,
					 kernel_size = (4,4),
					 padding = 'same'))
	model.add(BatchNormalization())
	model.add(Activation('relu'))
	model.add(Dropout(0.15))
	model.add(Conv2D(filters = 64, 
					 kernel_size = (3,3),
					 padding = 'same'))
	model.add(BatchNormalization())
	model.add(Activation('relu'))
	model.add(Dropout(0.15))
	model.add(Conv2D(filters = 32,
					 kernel_size = (3,3),
					 padding = 'same'))
	model.add(BatchNormalization())
	model.add(Activation('relu'))
	model.add(Dropout(0.15))
	model.add(Conv2D(filters = 16,
					 kernel_size = (3,3),
					 padding = 'same'))
	model.add(BatchNormalization())
	model.add(Activation('relu'))
	model.add(MaxPooling2D((2,2)))
	model.add(Dropout(0.35))
	model.add(Flatten())
	model.add(Dense(64, activation = 'relu'))
	model.add(Dense(10, activation = 'softmax'))

	return model