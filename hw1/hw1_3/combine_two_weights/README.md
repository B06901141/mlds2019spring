# README

###### tags: `MLDS` `hw1_3` `combine_two_weights`

## Toolkits
+ python 3.5.6
+ numpy 1.15.2
+ keras 2.2.2
+ tensorflow 1.10.0
+ matplotlib 3.0.0

## Regenerating
> Excute `python train3.py` to train, calculate points, plot  
> The result is in hw1_3/combine_two_weights/part3  

## subscript 
> all training using model2 in model3.py  
> weight1:batch_size=64, lr=0.001  
> weight2:batch_size=256, lr=0.001  
> weight3:batch_size=64, lr=0.01  
> figure3_1.png is using weights1 and 2 to plot.  
> figure3_2.png is using weights1 and 3 to plot.  
