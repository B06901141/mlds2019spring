import keras
import numpy as np
import os
from keras.optimizers import Adam

from model3 import model2
from graphic3 import plot_figure

def get_processed_data():
	from keras.datasets import cifar10
	from keras.utils import to_categorical
	(x_train, y_train), (x_test, y_test) = cifar10.load_data()
	x_train = x_train.astype('float32')/255
	x_test = x_test.astype('float32')/255
	y_train = to_categorical(y_train, 10)
	y_test = to_categorical(y_test, 10)
	return (x_train, y_train), (x_test, y_test)

def train_3_times(epochs = 40):
	if not os.path.isdir('./part3'):os.mkdir('part3')
	#data
	(x_train, y_train), (x_test, y_test) = get_processed_data()

	#first train
	model = model2(x_train.shape[1:])
	model.compile(loss = 'categorical_crossentropy',
				  optimizer = 'adam',
				  metrics = ['accuracy'])
	model.fit(x_train, y_train, 
			  batch_size = 64,
			  epochs = epochs,
			  verbose = 2,
			  validation_split = 0.2)
	model.save_weights("./part3/weights1.h5")

	#second train:batch_size=256
	model = model2(x_train.shape[1:])
	model.compile(loss = 'categorical_crossentropy',
				  optimizer = 'adam',
				  metrics = ['accuracy'])
	model.fit(x_train, y_train, 
	  		  batch_size = 256,
			  epochs = epochs,
			  verbose = 2,
			  validation_split = 0.2)
	model.save_weights("./part3/weights2.h5")

	#third train:lr=0.01
	model = model2(x_train.shape[1:])
	optimizer = Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
	model.compile(loss = 'categorical_crossentropy',
				  optimizer = optimizer,
				  metrics = ['accuracy'])
	model.fit(x_train, y_train, 
	  		  batch_size = 64,
			  epochs = epochs,
			  verbose = 2,
			  validation_split = 0.2)
	model.save_weights("./part3/weights3.h5")

def get_points(theta1=1, theta2=2, begin=-1, end=2.0, split=100):
	(x_train, y_train), (x_test, y_test) = get_processed_data()	
	model = model2(x_train.shape[1:])
	model.compile(loss = 'categorical_crossentropy',
				  optimizer = 'adam',
				  metrics = ['accuracy'])	
	model.load_weights("./part3/weights"+str(theta1)+".h5")
	weights1 = model.get_weights()
	model.load_weights('./part3/weights'+str(theta2)+'.h5')
	weights2 = model.get_weights()
	train_performances = []
	test_performances = []

	x_linspace = np.linspace(begin, end, split)
	for x in x_linspace:
		weight_now = [(1-x)*weights1[i]+x*weights2[i] for i in range(len(weights1))]
		model.set_weights(weight_now)
		train_performances.append(model.evaluate(x_train, y_train, batch_size=64))
		test_performances.append(model.evaluate(x_test, y_test, batch_size=64))

	np.save('./part3/x_linspace.npy', x_linspace)
	np.save('./part3/train_performances('+str(theta1)+', '+str(theta2)+').npy', np.array(train_performances))
	np.save('./part3/test_performances('+str(theta1)+', '+str(theta2)+').npy', np.array(test_performances))

if __name__ == '__main__':
	train_3_times(epochs = 40)
	get_points(theta1=1, theta2=2, begin=-1, end=2.0, split=100)
	get_points(theta1=1, theta2=3, begin=-1, end=2.0, split=100)
	plot_figure(theta1=1, theta2=2, name='./part3/figure3_1.png')
	plot_figure(theta1=1, theta2=3, name='./part3/figure3_2.png')
