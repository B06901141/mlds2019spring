# README

###### tags: `MLDS` `hw1_3` `numParam`

## Toolkits
+ python 3.5.5
+ numpy 1.14.6
+ tensorflow 1.8.0
+ keras 2.1.5

## Regenerating
> Excute `train.sh` to train model and store data points into [./output/](./output/)  
> Excute `graph.py` to draw graphics  
> The result is in [./graphic/](./graphic/)  
