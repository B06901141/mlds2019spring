import matplotlib.pyplot as plt
import numpy as np

from readWrite import *

def plotParam():
    plt.figure(figsize=(14,6))
    for j,figureName in zip(range(1,3),["loss","acc"]):
        plt.subplot(1,2,j)
        for i in ["train_","test_"]:
            path = "output/" + i + figureName + '.csv'
            label = i + figureName
            result = readTxt(path)
            plt.plot(result[:,0],result[:,1],'o',label = label)
        if figureName == "acc":
            figureName = "accuracy"
        plt.legend(loc = "best")
        plt.xlabel("number of parameters")
        plt.ylabel(figureName)
        plt.title("model " + figureName)
    plt.tight_layout()
    make_folder("graphic")
    plt.savefig("graphic/result.jpg")
    plt.cla()
    plt.clf()
    plt.close()

if __name__ == '__main__':
    plotParam()
