import os

import keras
import numpy as np
from keras.datasets import cifar10
from keras.layers import (Activation, BatchNormalization, Conv2D, Dense,
                          Dropout, Flatten, MaxPooling2D)
from keras.models import Sequential

from readWrite import *

if __name__ == '__main__':

    num_classes = 10
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    start_point = [4,14]
    for dim in range(start_point[0],129,2):
        for ratio in range(2,17,4):
            if dim == start_point[0] and ratio < start_point[1]:
                break
            model = Sequential()
            model.add(Conv2D(dim, (3, 3), padding='same',
                              input_shape=x_train.shape[1:]))
            model.add(BatchNormalization())
            model.add(Activation('relu'))
            model.add(Dropout(0.3))
            model.add(Conv2D(dim, (3, 3), padding='same'))
            model.add(BatchNormalization())
            model.add(Activation('relu'))
            model.add(Dropout(0.4))
            model.add(MaxPooling2D(pool_size=(2, 2)))

            model.add(Conv2D(dim*2, (3, 3), padding='same'))
            model.add(BatchNormalization())
            model.add(Activation('relu'))
            model.add(Dropout(0.4))
            model.add(Conv2D(dim*4, (3, 3), padding='same'))
            model.add(BatchNormalization())
            model.add(Activation('relu'))
            model.add(Dropout(0.4))
            model.add(Conv2D(dim*4, (3, 3), padding='same'))
            model.add(BatchNormalization())
            model.add(Activation('relu'))
            model.add(MaxPooling2D(pool_size=(2, 2)))

            model.add(Conv2D(dim*8, (3, 3), padding='same'))
            model.add(BatchNormalization())
            model.add(Activation('relu'))
            model.add(Dropout(0.4))
            model.add(Conv2D(dim*8, (3, 3), padding='same'))
            model.add(BatchNormalization())
            model.add(Activation('relu'))
            model.add(Dropout(0.4))
            model.add(Conv2D(dim*8, (3, 3), padding='same'))
            model.add(BatchNormalization())
            model.add(Activation('relu'))
            model.add(MaxPooling2D(pool_size=(2, 2)))

            model.add(Flatten())
            model.add(Dropout(0.5))
            model.add(Dense(dim*ratio, activation='relu'))
            model.add(BatchNormalization())
            model.add(Dropout(0.5))
            model.add(Dense(10, activation='softmax'))

            model.compile(loss="categorical_crossentropy",
                           optimizer='adam',
                           metrics=["accuracy"])
            param_num = model.count_params()
            model.fit(x_train, y_train, epochs=60, validation_split=0.2)
            out = model.evaluate(x_train,y_train)
            make_folder("output")
            appendTxt([param_num,out[0]],"output/train_loss.csv")
            appendTxt([param_num,out[1]],"output/train_acc.csv")
            out = model.evaluate(x_test,y_test)
            appendTxt([param_num,out[0]],"output/test_loss.csv")
            appendTxt([param_num,out[1]],"output/test_acc.csv")
            with open("output/out.log",'a') as file1:
                file1.write(str(dim) + "," + str(ratio) + "\n")
            del model
