import os

import numpy as np


def make_folder(folderName):
    to_make = folderName.split('/')
    now = to_make[0]
    for i in to_make[1:]+['']:
        try:
            os.mkdir(now)
        except:
            pass
        now += '/'+i


def writeTxt(arr, fileName,mode = 'w'):
    check(fileName)
    temp = np.array(arr, dtype='str')
    if len(temp.shape) == 1:
        temp = temp.reshape(1, -1)
    with open(fileName, mode) as file1:
        for i in temp:
            file1.write(','.join(i)+'\n')
def appendTxt(arr,fileName):
    writeTxt(arr,fileName,mode = 'a')

def readTxt(fileName):
    with open(fileName, 'r') as file1:
        s = file1.read()
    data = np.array([i.split(',')
                     for i in s.split('\n')[:-1]], dtype='float64')
    return data


def check(path):
    if '\\' in path:
        path = path.replace('\\', '/')
    if '/' in path:
        make_folder('/'.join(path.split('/')[:-1]))


if __name__ == '__main__':
    a = np.array([[1,2],[3,4]])
    appendTxt(a,'test.csv')
