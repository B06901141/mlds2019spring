# README

###### tags: `MLDS` `hw1_3` `flatness_batchsize`

## Toolkits
+ python 3.5.5
+ numpy 1.14.6
+ tensorflow 1.8.0
+ keras 2.1.5

## Regenerating
> Excute `train.py` to train model, and the graphics will be stored into [./graphic/](./graphic/) automatically.  
> Argparse:  
>> `-d` or `--device`: default = "cuda"  
>> `-e` or `--epochs`: default = 60  
>> `-lr` or  `--learning_rate`: default = 1e-3  
>> `-mg` or  `--max_grad_norm`: Argument max_norm in torch.nn.utils.clip_grad when calculating sensitivity, default = 1  
>> `-x` or `--xscale`: The xscale of outpure figure, default = "log"  
>> `-y` or `--yscale`: The yscale of outpure figure, default = "linear"  
>> `-s` or `--start`: The lower endpoint of batch_size, default = 2  
>> `-r` or `--rate`: The ascending rate of batch_size, default = 1.1  
>> `-n` or `--end`: The higher endpoint of batch_size, default = 10000  
