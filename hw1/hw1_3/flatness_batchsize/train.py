import os
import pickle
import time
from argparse import ArgumentParser
from collections import namedtuple
from os import path

import numpy as np
import torch
import torch.nn.functional as F
from keras.datasets import mnist
from matplotlib import pyplot as plt
from PIL import Image
from torch import cuda, nn, optim
from torch.nn.utils import clip_grad_norm_
from torch.utils import data
from torchvision import datasets
from torchvision import transforms as T

from resnet import ResNet18

plt.switch_backend('agg')

MODEL_DIR = "./models"
DATASET_DIR = "./datasets"

Entry = namedtuple(typename="History", field_names=[
                   "batch", "loss", "acc", "val_loss", "val_acc", "norm", "val_norm"])


def download():
    root = DATASET_DIR
    datasets.CIFAR10(root=root, download=True)
    datasets.MNIST(root=root, download=True)
    return root


def clip_grad(net, max_grad_norm):
    for params in net.parameters():
        clip_grad_norm_(parameters=params, max_norm=max_grad_norm)


def categorical_accuracy(input, target):
    input = input.detach().cpu().numpy()
    target = target.detach().cpu().numpy()
    return (input.argmax(-1) == target).mean()


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 20, 5, 1)
        self.conv2 = nn.Conv2d(20, 50, 5, 1)
        self.fc1 = nn.Linear(4*4*50, 500)
        self.fc2 = nn.Linear(500, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.dropout2d(x, p=.5)
        x = F.relu(self.conv2(x))
        x = F.dropout(x, p=.5)
        x = F.max_pool2d(x, 2, 2)
        x = x.view(-1, 4*4*50)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


class MNIST(data.Dataset):
    def __init__(self, train, device, *args, **kwargs):
        super().__init__(*args, **kwargs)
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        if train:
            x = x_train
            y = y_train
        else:
            x = x_test
            y = y_test
        assert len(x) == len(y)
        x = np.expand_dims(x, 1).astype(np.float)
        x /= 255
        self.x = torch.tensor(x, device=device).float()
        self.y = torch.tensor(y, device=device).long()

    def __len__(self):
        return len(self.y)

    def __getitem__(self, index):
        return self.x[index], self.y[index]


def train(epochs, batch_size, data, model, optimizer, loss_fn, max_grad_norm):
    model.train()
    for epo in range(1, 1+epochs):
        total_loss = 0
        start = time.time()
        for i, batch in enumerate(data, 1):
            if i % 5 == 0:
                print("Train batch {}".format(i), end='\r')
            input, target = batch
            output = model(input)
            loss = loss_fn(input=output, target=target)
            optimizer.zero_grad()
            loss.backward()
            clip_grad(model, max_grad_norm)
            optimizer.step()
            total_loss += loss.item()
        print("Epoch {0:03d} | Batch {1} | Loss {2:.3f} | Time {3:.3f}".
              format(epo, batch_size, total_loss, time.time()-start), flush=True)


def eval_on(train, test, model):
    model.eval()
    with torch.no_grad():
        # train
        train_acc = []
        train_loss = []
        for i, batch in enumerate(train, 1):
            print("Eval train batch {}".format(i), end='\r')
            input, target = batch
            output = model(input)
            train_loss.append(F.nll_loss(output, target))
            train_acc.append(categorical_accuracy(output, target))
        train_acc = sum(train_acc)/len(train_acc)
        train_loss = (sum(train_loss)/len(train_loss)).item()
        # test
        test_acc = []
        test_loss = []
        for i, batch in enumerate(test, 1):
            print("Eval test batch {}".format(i), end='\r')
            input, target = batch
            output = model(input)
            test_loss.append(F.nll_loss(output, target))
            test_acc.append(categorical_accuracy(output, target))
        test_acc = sum(test_acc)/len(test_acc)
        test_loss = (sum(test_loss)/len(test_loss)).item()
    return (train_acc, test_acc), (train_loss, test_loss)


def input_norm(train, test, model):
    model.eval()
    train_norm = []
    test_norm = []
    for i, batch in enumerate(train, 1):
        input, target = batch
        input.requires_grad_()
        output = model(input)
        model.zero_grad()
        None if input.grad is None else input.grad.zero_()
        F.nll_loss(output, target).backward()
        train_norm.append(input.grad.norm().item())
    for i, batch in enumerate(test, 1):
        input, target = batch
        input.requires_grad_()
        output = model(input)
        model.zero_grad()
        None if input.grad is None else input.grad.zero_()
        F.nll_loss(output, target).backward()
        test_norm.append(input.grad.norm().item())
    train_norm = sum(train_norm)/len(train_norm)
    test_norm = sum(test_norm)/len(test_norm)
    return train_norm, test_norm


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-d', "--device", type=str, default="cuda")
    parser.add_argument('-e', "--epochs", type=int, default=60)
    parser.add_argument('-lr', "--learning_rate", type=float, default=1e-3)
    parser.add_argument('-mg', "--max_grad_norm", type=float, default=1)
    parser.add_argument('-x', "--xscale", type=str, default="log")
    parser.add_argument('-y', "--yscale", type=str, default="linear")
    parser.add_argument('-s', "--start", type=int, default=2)
    parser.add_argument('-r', "--rate", type=float, default=1.1)
    parser.add_argument('-n', "--end", type=int, default=10000)

    options = parser.parse_args()

    options.device = options.device if cuda.is_available() else "cpu"
    xscale = options.xscale
    yscale = options.yscale

    os.makedirs(MODEL_DIR, exist_ok=True)
    os.makedirs("graphic", exist_ok=True)

    # transform = T.ToTensor()
    # train_dataset = CIFAR10(
    #     root=DATASET_DIR, train=True, transform=transform, device=options.device)
    # test_dataset = CIFAR10(
    #     root=DATASET_DIR, train=False, transform=transform, device=options.device)
    train_dataset = MNIST(True, options.device)
    test_dataset = MNIST(False, options.device)

    test_loader = data.DataLoader(test_dataset, batch_size=len(test_dataset))

    history = []

    current = options.start
    step = options.rate
    batch_size = int(current)

    try:
        while batch_size <= options.end:
            model = Net().to(options.device).float()
            optimizer = optim.Adam(
                model.parameters(), lr=options.learning_rate)
            train_loader = data.DataLoader(
                train_dataset, batch_size=batch_size, shuffle=True)

            train(epochs=options.epochs,
                  batch_size=batch_size,
                  data=train_loader,
                  model=model,
                  optimizer=optimizer,
                  loss_fn=F.nll_loss,
                  max_grad_norm=options.max_grad_norm)
            torch.save(obj=model, f=path.join(
                MODEL_DIR, "{0}.pt".format(batch_size)))
            (train_acc, test_acc), (train_loss, test_loss) =\
                eval_on(train_loader, test_loader, model)

            train_norm, test_norm = input_norm(
                train_loader, test_loader, model)

            history.append(Entry(batch=batch_size,
                                 loss=train_loss, val_loss=test_loss,
                                 acc=train_acc, val_acc=test_acc,
                                 norm=train_norm, val_norm=test_norm))

            temp_batch = batch_size
            while batch_size == temp_batch:
                current *= step
                batch_size = int(current)

    except KeyboardInterrupt as k:
        print(k)
    except RuntimeError as e:
        print(e)
    finally:
        print()

    plt.clf()
    plt.title("acc")
    plt.xscale(xscale)
    plt.yscale(yscale)
    plt.scatter(tuple(ent.batch for ent in history),
                tuple(ent.acc for ent in history), label="train")
    plt.scatter(tuple(ent.batch for ent in history),
                tuple(ent.val_acc for ent in history), label="test")
    plt.legend()
    plt.savefig("./graphic/acc_"+str(options.learning_rate)+".png")

    plt.clf()
    plt.title("loss")
    plt.xscale(xscale)
    plt.yscale(yscale)
    plt.scatter(tuple(ent.batch for ent in history),
                tuple(ent.loss for ent in history), label="train")
    plt.scatter(tuple(ent.batch for ent in history),
                tuple(ent.val_loss for ent in history), label="test")
    plt.legend()
    plt.savefig("./graphic/loss_"+str(options.learning_rate)+".png")

    plt.clf()
    plt.title("sens")
    plt.xscale(xscale)
    plt.yscale(yscale)
    plt.scatter(tuple(ent.batch for ent in history),
                tuple(ent.norm for ent in history), label="train")
    plt.scatter(tuple(ent.batch for ent in history),
                tuple(ent.val_norm for ent in history), label="test")
    plt.legend()
    plt.savefig("graphic/sens_"+str(options.learning_rate)+".png")
