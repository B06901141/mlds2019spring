# README

###### tags: `MLDS` `hw1_3` `random labels`

## Toolkits
+ python 3.6.8
+ numpy 1.16.2
+ keras 2.2.4
+ tensorflow 1.12.0
+ matplotlib 3.0.3

## Regenerating
> Excute `python train.py` to train MNIST
> Excute `python graph.py` to draw picture
> The result is in hw1_1/simulation/graphic
