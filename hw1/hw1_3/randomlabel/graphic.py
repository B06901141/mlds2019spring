import matplotlib.pyplot as plt
import numpy as np
import os

loss = 'loss'
acc = 'acc'
val_loss = 'val_loss'
val_acc = 'val_acc'
test_loss = 'test_loss'
test_acc = 'test_acc'

def readTxt(fileName):
    with open(fileName, 'r') as file1:
        return [float(d) for d in file1.read().rstrip('\n').split(',')]
     

def plot(task):
    y = readTxt('output/' + task + '.csv')
    x = list(range(1, len(y) + 1))
    plt.plot(x, y)
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.title(task)
    # plt.show()
    plt.savefig('graphic/' + task + '.jpg')
    plt.cla()
    plt.clf()
    plt.close()

def plotResult(task):
    y1 = readTxt('output/' + task + '.csv')
    y2 = readTxt('output/test_' + task + '.csv')
    y3 = readTxt('output/val_' + task + '.csv')
    x = list(range(1, len(y1) + 1))
    plt.plot(x, y1)
    plt.plot(x, y2)
    plt.plot(x, y3)
    plt.xlabel('epoch')
    plt.ylabel(task)
    plt.title(task + '-epochs')
    plt.legend(loc='best', labels=[task, 'test_' + task, 'val_' + task])
    plt.savefig('graphic/' + task + '_epochs.png')
    plt.close()

for i in [acc, loss, val_acc, val_loss, test_acc, test_loss]:
    plot(i)

plotResult('acc')
plotResult('loss')