from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, BatchNormalization
from keras.utils import to_categorical
from keras.callbacks import Callback
import numpy as np

current_dir = "/content/gdrive/My Drive/Colab Notebooks/mlds/"

class Evaluator(Callback):
    def __init__(self, train_data):
        self.x_test, self.y_test = train_data
        self.loss = []
        self.acc = []

    def on_epoch_end(self, epoch, logs={}):
        loss, acc = self.model.evaluate(self.x_test, self.y_test, verbose=0)
        self.loss.append(loss)
        self.acc.append(acc)
        print('test_loss: %f - test_acc: %f' % (loss, acc))

def writeTxt(arr,fileName):
    temp = np.array(arr,dtype = 'str')
    if len(temp.shape) == 1:
        temp = temp.reshape(1,-1)
    with open(fileName,'w') as file1:
        for i in temp:
            file1.write(','.join(i)+'\n')

if __name__ == '__main__':
    batch_size = 128
    num_classes = 10
    epochs = 12

    size = 10000
    testsize = 1000
    # input image dimensions
    img_rows, img_cols = 28, 28

    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    ch = range(10)
    np.random.shuffle(y_train)
    # y_train = [np.random.choice(ch) for _ in y_train]

    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')

    y_train = to_categorical(y_train, num_classes)
    y_test = to_categorical(y_test, num_classes)

    model = Sequential()

    model.add(Conv2D(32, (3, 3), padding = 'same', input_shape=x_train.shape[1:], kernel_initializer='orthogonal'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    # model.add(Dropout(0.1))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32,(3, 3), padding='same',
                 input_shape=x_train.shape[1:], kernel_initializer='orthogonal'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    # model.add(Dropout(0.1))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32,(3, 3), padding='same',
                 input_shape=x_train.shape[1:], kernel_initializer='orthogonal'))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    # model.add(Dropout(0.1))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())    
    # model.add(Dropout(0.1))
    model.add(Dense(50,activation = 'relu', kernel_initializer='orthogonal'))
    model.add(BatchNormalization())
    model.add(Dense(50,activation = 'relu', kernel_initializer='orthogonal'))
    model.add(BatchNormalization())
    # model.add(Dropout(0.1))
    model.add(Dense(10,activation = 'sigmoid'))


    model.compile(loss="categorical_crossentropy",
              optimizer='adam',
              metrics=["accuracy"])
    model.summary()
    eva = Evaluator((x_test, y_test))
    history = model.fit(x_train, y_train, epochs=600, validation_split=0.1, batch_size=batch_size,  \
              callbacks=[eva])
    model.save("%smodel.hdf5" % current_dir)

    writeTxt(eva.acc, '%stest_acc.csv' % current_dir)
    writeTxt(eva.loss, '%stest_loss.csv' % current_dir)
    writeTxt(history.history['loss'], '%sloss.csv' % current_dir)
    writeTxt(history.history['val_loss'], '%sval_loss.csv' % current_dir)
    writeTxt(history.history['acc'], '%sacc.csv' % current_dir)
    writeTxt(history.history['val_acc'], '%sval_acc.csv' % current_dir)

