import matplotlib.pyplot as plt
import numpy as np

from dataSet import *
from readWrite import *


def plot_result(task):
    from keras.models import load_model
    make_folder("graphic")

    x, y = readData("data/"+task+".csv")

    plt.plot(x, y)
    for i in range(1, 4):
        model = load_model("model/"+task+"/model"+str(i)+"/model"+str(i)+".h5")
        y = model.predict(x)
        plt.plot(x, y, label="model"+str(i))
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title(task)
    plt.legend(loc='best')
    plt.savefig("graphic/"+task+".jpg")
    plt.cla()
    plt.clf()
    plt.close()


def plot_loss(task):
    make_folder("graphic")
    for j in ["loss", "val_loss"]:
        for i in range(1, 4):
            x = readTxt("model/"+task+"/model"+str(i)+"/"+j+".txt").flatten()
            plt.plot(x, label="model"+str(i))
        plt.xlabel("epoch")
        plt.ylabel(j)
        plt.title(task + "_"+j)
        plt.legend(loc='best')
        plt.savefig("graphic/"+task+"_"+j+".jpg")
        plt.cla()
        plt.clf()
        plt.close()


if __name__ == '__main__':
    plot_result("sinc")
    plot_result("square")
    plot_loss("sinc")
    plot_loss("square")
