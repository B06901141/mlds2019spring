import os

import numpy as np
from keras.layers import Dense
from keras.models import Sequential

from dataSet import *
from readWrite import *

if __name__ == '__main__':

    for i in ['sinc', 'square']:
        x, y = readTrain("data/"+i+".csv")
        make_folder("model/"+i+"/model1")
        model1 = Sequential()
        model1.add(Dense(units=10000, activation='relu', input_dim=1))
        model1.add(Dense(units=1))
        model1.compile(loss='mse',
                       optimizer='adam',
                       metrics=[])
        model1.summary()
        history1 = model1.fit(x, y, epochs=60, validation_split=0.2)
        model1.save("model/"+i+"/model1/model1.h5")
        writeTxt(history1.history["loss"], "model/"+i+"/model1/loss.txt")
        writeTxt(history1.history["val_loss"],
                 "model/"+i+"/model1/val_loss.txt")

        make_folder("model/"+i+"/model2")
        model2 = Sequential()
        model2.add(Dense(units=85, activation='relu', input_dim=1))
        model2.add(Dense(units=86, activation='relu'))
        model2.add(Dense(units=86, activation='relu'))
        model2.add(Dense(units=86, activation='relu'))
        model2.add(Dense(units=85, activation='relu'))
        model2.add(Dense(units=1))
        model2.compile(loss='mse',
                       optimizer='adam',
                       metrics=[])
        model2.summary()
        history2 = model2.fit(x, y, epochs=60, validation_split=0.2)
        model2.save("model/"+i+"/model2/model2.h5")
        writeTxt(history2.history["loss"], "model/"+i+"/model2/loss.txt")
        writeTxt(history2.history["val_loss"],
                 "model/"+i+"/model2/val_loss.txt")

        make_folder("model/"+i+"/model3")
        model3 = Sequential()
        model3.add(Dense(units=58, activation='relu', input_dim=1))
        model3.add(Dense(units=57, activation='relu'))
        model3.add(Dense(units=57, activation='relu'))
        model3.add(Dense(units=57, activation='relu'))
        model3.add(Dense(units=57, activation='relu'))
        model3.add(Dense(units=57, activation='relu'))
        model3.add(Dense(units=57, activation='relu'))
        model3.add(Dense(units=57, activation='relu'))
        model3.add(Dense(units=57, activation='relu'))
        model3.add(Dense(units=57, activation='relu'))
        model3.add(Dense(units=1))
        model3.compile(loss='mse',
                       optimizer='adam',
                       metrics=[])

        model3.summary()
        history3 = model3.fit(x, y, epochs=60, validation_split=0.2)
        model3.save("model/"+i+"/model3/model3.h5")
        writeTxt(history3.history["loss"], "model/"+i+"/model3/loss.txt")
        writeTxt(history3.history["val_loss"],
                 "model/"+i+"/model3/val_loss.txt")
