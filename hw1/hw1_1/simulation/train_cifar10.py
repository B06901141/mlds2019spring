import os

import numpy as np
from keras.layers import Dense, Activation, Convolution2D, MaxPooling2D, Flatten, Dropout
from keras.models import Sequential
from keras.datasets import cifar10
from keras.utils import to_categorical

from dataSet import *
from readWrite import *

if __name__ == '__main__':
	epochs = 1
	#get data
	(x_train, y_train), (x_test, y_test) = cifar10.load_data()
	x_train = x_train.astype('float32')
	x_test = x_test.astype('float32')
	x_train /= 255
	x_test /= 255

	# Convert class vectors to binary class matrices.
	y_train = to_categorical(y_train, 10)
	y_test = to_categorical(y_test, 10)

	make_folder("model/"+"cifar10"+"/model1")
	model1 = Sequential()
	model1.add(Convolution2D(filters = 8, 
							kernel_size =(3,3), 
							padding='same',
							input_shape=x_train.shape[1:]
	))
	model1.add(Activation('relu')) 
	model1.add(MaxPooling2D(pool_size=(2, 2)))
	model1.add(Dropout(0.25))
	model1.add(Flatten())

	model1.add(Dense(10))
	model1.add(Activation('softmax'))

	model1.compile(loss='categorical_crossentropy',
				   optimizer='adam',
				   metrics=['accuracy'])
	model1.summary()

	history1 = model1.fit(x_train, y_train, batch_size=32, epochs=epochs, validation_split=0.2)
	model1.save("model/"+"cifar10"+"/model1/model1.h5")
	writeTxt(history1.history["loss"], "model/"+"cifar10"+"/model1/loss.txt")
	writeTxt(history1.history["val_loss"],
			 "model/"+"cifar10"+"/model1/val_loss.txt")


	make_folder("model/"+"cifar10"+"/model2")
	model2 = Sequential()
	model2.add(Convolution2D(filters = 4, 
							kernel_size =(3,3), 
							padding='same',
							input_shape=x_train.shape[1:]
	))
	model2.add(Activation('relu')) 
	model2.add(Convolution2D(8,(3,3),padding='same'))
	model2.add(Activation('relu')) 
	model2.add(MaxPooling2D(pool_size=(2, 2)))
	model2.add(Dropout(0.25))
	model2.add(Flatten())

	model2.add(Dense(10))
	model2.add(Activation('softmax'))

	model2.compile(loss='categorical_crossentropy',
				   optimizer='adam',
				   metrics=['accuracy'])
	model2.summary()

	history2 = model2.fit(x_train, y_train, batch_size=32, epochs=epochs, validation_split=0.2)
	model2.save("model/"+"cifar10"+"/model2/model2.h5")
	writeTxt(history2.history["loss"], "model/"+"cifar10"+"/model2/loss.txt")
	writeTxt(history2.history["val_loss"],
			 "model/"+"cifar10"+"/model2/val_loss.txt")


	make_folder("model/"+"cifar10"+"/model3")
	model3 = Sequential()
	model3.add(Convolution2D(filters = 16, 
							kernel_size =(3,3), 
							padding='same',
							input_shape=x_train.shape[1:]
	))
	model3.add(Activation('relu')) 
	model3.add(Convolution2D(32,(3,3),padding='same'))
	model3.add(Activation('relu')) 
	model3.add(MaxPooling2D(pool_size=(2, 2)))
	model3.add(Dropout(0.25))
	model3.add(Convolution2D(12,(3,3),padding='same'))
	model3.add(Activation('relu'))
	model3.add(Convolution2D(8,(3,3),padding='same'))
	model3.add(Activation('relu'))  
	model3.add(MaxPooling2D(pool_size=(2, 2)))
	model3.add(Dropout(0.25))
	model3.add(Convolution2D(16,(3,3),padding='same'))
	model3.add(Activation('relu')) 
	model3.add(Flatten())

	model3.add(Dense(10))
	model3.add(Activation('softmax'))

	model3.compile(loss='categorical_crossentropy',
				   optimizer='adam',
				   metrics=['accuracy'])
	model3.summary()

	history3 = model3.fit(x_train, y_train, batch_size=32, epochs=epochs, validation_split=0.2)
	model3.save("model/"+"cifar10"+"/model3/model3.h5")
	writeTxt(history3.history["loss"], "model/"+"cifar10"+"/model3/loss.txt")
	writeTxt(history3.history["val_loss"],
			 "model/"+"cifar10"+"/model3/val_loss.txt")
