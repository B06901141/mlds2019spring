#!/bin/bash
if [ ! -e model ]
then
    if [ ! -e model.zip ]
    then
        wget -O "model.zip" "https://www.dropbox.com/s/gi3umd10blkc2rx/model_hw1_1.zip?dl=0"
    fi
    unzip "model.zip"
fi
python3 graphic.py
