import os

import keras
import numpy as np
from keras.datasets import cifar10
from keras.layers import (Activation, BatchNormalization, Conv2D, Dense,
                          Dropout, Flatten, MaxPooling2D)
from keras.models import Sequential

from readWrite import *

if __name__ == '__main__':

    num_classes = 10
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    make_folder("model/model1")
    model1 = Sequential()
    model1.add(Conv2D(256, (3, 3), padding='same',
                      input_shape=x_train.shape[1:]))
    model1.add(BatchNormalization())
    model1.add(Activation('relu'))
    model1.add(Dropout(0.3))
    model1.add(MaxPooling2D(pool_size=(2, 2)))

    model1.add(Flatten())
    model1.add(Dropout(0.5))
    model1.add(Dense(170, activation='relu'))
    model1.add(BatchNormalization())
    model1.add(Dropout(0.5))
    model1.add(Dense(10, activation='softmax'))

    model1.compile(loss="categorical_crossentropy",
                   optimizer='adam',
                   metrics=["accuracy"])
    model1.summary()
    history1 = model1.fit(x_train, y_train, epochs=60, validation_split=0.2)
    model1.save("model/model1/model1.h5")
    writeTxt(history1.history["loss"], "model/model1/loss.txt")
    writeTxt(history1.history["val_loss"], "model/model1/val_loss.txt")
    writeTxt(history1.history["acc"], "model/model1/acc.txt")
    writeTxt(history1.history["val_acc"], "model/model1/val_acc.txt")
#######################################################################################
    make_folder("model/model2")
    model2 = Sequential()
    model2.add(Conv2D(64, (3, 3), padding='same',
                      input_shape=x_train.shape[1:]))
    model2.add(BatchNormalization())
    model2.add(Activation('relu'))
    model2.add(Dropout(0.3))
    model2.add(MaxPooling2D(pool_size=(2, 2)))

    model2.add(Conv2D(256, (3, 3), padding='same'))
    model2.add(BatchNormalization())
    model2.add(Activation('relu'))
    model2.add(Dropout(0.4))
    model2.add(MaxPooling2D(pool_size=(2, 2)))
    model2.add(Conv2D(1024, (3, 3), padding='same'))
    model2.add(BatchNormalization())
    model2.add(Activation('relu'))
    model2.add(Dropout(0.4))
    model2.add(MaxPooling2D(pool_size=(2, 2)))

    model2.add(Flatten())
    model2.add(Dropout(0.5))
    model2.add(Dense(512, activation='relu'))
    model2.add(BatchNormalization())
    model2.add(Dropout(0.5))
    model2.add(Dense(10, activation='softmax'))

    model2.compile(loss="categorical_crossentropy",
                   optimizer='adam',
                   metrics=["accuracy"])
    model2.summary()
    history1 = model2.fit(x_train, y_train, epochs=60, validation_split=0.2)
    model2.save("model/model2/model2.h5")
    writeTxt(history1.history["loss"], "model/model2/loss.txt")
    writeTxt(history1.history["val_loss"], "model/model2/val_loss.txt")
    writeTxt(history1.history["acc"], "model/model2/acc.txt")
    writeTxt(history1.history["val_acc"], "model/model2/val_acc.txt")
###############################################################################################################

    make_folder("model/model3")
    model3 = Sequential()
    model3.add(Conv2D(64, (3, 3), padding='same',
                      input_shape=x_train.shape[1:]))
    model3.add(BatchNormalization())
    model3.add(Activation('relu'))
    model3.add(Dropout(0.3))
    model3.add(Conv2D(64, (3, 3), padding='same'))
    model3.add(BatchNormalization())
    model3.add(Activation('relu'))
    model3.add(Dropout(0.4))
    model3.add(MaxPooling2D(pool_size=(2, 2)))

    model3.add(Conv2D(128, (3, 3), padding='same'))
    model3.add(BatchNormalization())
    model3.add(Activation('relu'))
    model3.add(Dropout(0.4))
    model3.add(Conv2D(256, (3, 3), padding='same'))
    model3.add(BatchNormalization())
    model3.add(Activation('relu'))
    model3.add(Dropout(0.4))
    model3.add(Conv2D(256, (3, 3), padding='same'))
    model3.add(BatchNormalization())
    model3.add(Activation('relu'))
    model3.add(MaxPooling2D(pool_size=(2, 2)))

    model3.add(Conv2D(512, (3, 3), padding='same'))
    model3.add(BatchNormalization())
    model3.add(Activation('relu'))
    model3.add(Dropout(0.4))
    model3.add(Conv2D(512, (3, 3), padding='same'))
    model3.add(BatchNormalization())
    model3.add(Activation('relu'))
    model3.add(Dropout(0.4))
    model3.add(Conv2D(512, (3, 3), padding='same'))
    model3.add(BatchNormalization())
    model3.add(Activation('relu'))
    model3.add(MaxPooling2D(pool_size=(2, 2)))

    model3.add(Flatten())
    model3.add(Dropout(0.5))
    model3.add(Dense(512, activation='relu'))
    model3.add(BatchNormalization())
    model3.add(Dropout(0.5))
    model3.add(Dense(10, activation='softmax'))

    model3.compile(loss="categorical_crossentropy",
                   optimizer='adam',
                   metrics=["accuracy"])
    model3.summary()
    history1 = model3.fit(x_train, y_train, epochs=60, validation_split=0.2)
    model3.save("model/model3/model3.h5")
    writeTxt(history1.history["loss"], "model/model3/loss.txt")
    writeTxt(history1.history["val_loss"], "model/model3/val_loss.txt")
    writeTxt(history1.history["acc"], "model/model3/acc.txt")
    writeTxt(history1.history["val_acc"], "model/model3/val_acc.txt")
