import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np

from readWrite import *


def plot_loss():
    make_folder("graphic")
    for j, k in zip(["loss", "acc", "val_loss", "val_acc"], range(1, 5)):
        plt.subplot(2, 2, k)
        for i in range(1, 4):
            x = readTxt("model/model"+str(i)+"/"+j+".txt").flatten()
            plt.plot(x, label="model"+str(i))
        plt.xlabel("epoch")
        plt.ylabel(j)
        plt.legend(loc='best')
        plt.title(j)
    plt.suptitle("training_history")
    plt.tight_layout()
    plt.savefig("graphic/training_history.jpg")
    plt.cla()
    plt.clf()
    plt.close()


if __name__ == '__main__':
    plot_loss()
