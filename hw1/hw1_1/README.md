# HW 1-1

* Author: 齊湣王/孟嘗君/馮諼

## Toolkits ##
* numpy 1.14.6
* tensorflow 1.8.0
* keras 2.1.5

## Regenerating ##

### Simulation ###
>Execute hw1_1/simulation/all.sh -> this will RETRAIN all models  
>Execute hw1_1/simulation/half.sh -> this regenerates graphics only  
>The result is in hw1_1/simulation/graphic  

### Actual Task ###
>Execute hw1_1/actual_task_CIFAR10/all.sh -> this will RETRAIN all models  
>Execute hw1_1/actual_task_CIFAR10/half.sh -> this regenerates graphics only  
>The result is in hw1_1/actual_task_CIFAR10/graphic  
