import os

import numpy as np


from keras.callbacks import ModelCheckpoint, LambdaCallback ,Callback
import keras.backend as K
from keras.losses import mean_squared_error
from keras.optimizers import RMSprop, Adam, SGD


from dataSet import *
from readWrite import *
from my_model import *


def log_save(model, p_history, history, path):
    model.save(path+"/model.h5")
    writeTxt(p_history.loss_per_batch, path+"/loss.txt")
    writeTxt(history.history["val_loss"],path+"/val_loss.txt")
    writeTxt(p_history.p_norm, path+"/p_norm.txt")
    model.save_weights(save_path+"weights.hdf5")

class p_norm_History(Callback):
    def on_train_begin(self, logs={}):
        self.p_norm = []
        self.loss_per_batch = []

    def on_batch_end(self, batch, logs={}):
        self.p_norm.append(logs.get('p_norm'))
        self.loss_per_batch.append(logs.get('loss'))


if __name__ == '__main__':
    #event_num = (1,10)#event1 to 10:eopchs=60, after changing objective function epochs=200
    #event_num = (11,20)#event11 to 20:eopchs=30, after changing objective function epochs=50
    #event_num = (21,30)#event21 to 30:eopchs=60, after changing objective function epochs=50
    #event_num = (31,40)#event31 to 40:eopchs=60, after changing objective function epochs=70
    #event_num = (41,50)#event41 to 50:eopchs=10, after changing objective function epochs=10
    #event_num = (51,60)#event51 to 60:eopchs=1, after changing objective function epochs=1
    event_num = (1,60)
    epochs = 60

    for i in ['sinc']:
        #readdata
        x, y = readTrain("data/"+i+".csv")
        for model_num in range(4,5):
            make_folder("model_norm/"+i+"/model"+str(model_num))
            for event in range(event_num[0],event_num[1]+1):
                #build folder
                save_path = "model_norm/"+i+"/model"+str(model_num)+"/event"+str(event)+"/"
                make_folder(save_path)
                    
                #model
                exec("model=model"+str(model_num)+"()")
                def p_norm(y_true, y_pred):
                    grads = K.gradients(mean_squared_error(y_true, y_pred), model.weights)
                    summed_squares = [K.sum(K.square(g)) for g in grads]
                    norm = K.sqrt(sum(summed_squares))
                    return norm

                model.compile(loss="mse",
                              optimizer='adam',
                              metrics=[p_norm])#remove p_norm
 
                #checkpoint
                p_history = p_norm_History()
                checkpoint = ModelCheckpoint(save_path+"best_model.hdf5", 
                                             monitor='p_norm',
                                             save_best_only=True,
                                             mode='min',
                                             verbose=0,
                                             save_weights_only=False,
                                             period=1)

                #fit
                history1 = model.fit(x, y,
                                    epochs=epochs,
                                    validation_split=0.2,
                                    verbose=2,
                                    callbacks=[checkpoint, p_history])

                #optimizer = Adam(lr=0.000001, beta_1=0.9, beta_2=0.999, epsilon=1e-08)#0.003
                #optimizer = SGD(lr=1e-7, momentum=0.3, decay=2e-3, nesterov=False)#0.03
                #optimizer = Adam(lr=1.5e-7, beta_1=0.9, beta_2=0.999, epsilon=1e-08)#0.033
                optimizer = Adam(lr=2e-7, beta_1=0.9, beta_2=0.999, epsilon=1e-08)#0.03
                model.compile(loss=p_norm,
                              optimizer=optimizer,
                              metrics=[p_norm])

                history2 = model.fit(x, y,
                                    epochs=200,
                                    validation_split=0.01,
                                    verbose=2,
                                    callbacks=[checkpoint, p_history])

                log_save(model, p_history, history1, save_path)
