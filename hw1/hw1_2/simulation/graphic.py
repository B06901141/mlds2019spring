import keras.backend as K
import matplotlib.pyplot as plt
import numpy as np
from keras.losses import mean_squared_error

from dataSet import *
from readWrite import *
from train import model1, model2, model3


def plot_result_new(folder, task, event):
    make_folder("graphic")
    make_folder("graphic/"+task)

    x, y = readData("data/"+task+".csv")
    plt.plot(x, y, label="origin")
    for i in range(1, 4):
        if i == 1:
            model = model1()
        elif i == 2:
            model = model2()
        elif i == 3:
            model = model3()

        def p_norm(y_true, y_pred):
            grads = K.gradients(mean_squared_error(
                y_true, y_pred), model.trainable_weights)
            summed_squares = [K.sum(K.square(g)) for g in grads]
            norm = K.sqrt(sum(summed_squares))
            return norm
        model.compile(loss='mse',
                      optimizer='adam',
                      metrics=['accuracy', p_norm])
        model.load_weights(folder+"/"+task+"/model"+str(i) +
                           "/event"+str(event)+"/weights.hdf5")
        y = model.predict(x)
        plt.plot(x, y, label="model"+str(i))
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title(task+"/event"+str(event))
    plt.legend(loc='best')
    plt.savefig("graphic/"+task+"/predict(event"+str(event)+").jpg")
    plt.cla()
    plt.clf()
    plt.close()


def plot_result(task, event):
    from keras.models import load_model
    make_folder("graphic")

    x, y = readData("data/"+task+".csv")

    plt.plot(x, y)
    for i in range(1, 4):
        model = load_model("model/"+task+"/model"+str(i) +
                           "/event"+str(event)+"/model"+str(i)+".h5")
        y = model.predict(x)
        plt.plot(x, y, label="model"+str(i))
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title(task+"/event"+str(event))
    plt.legend(loc='best')
    plt.savefig("graphic/"+task+".jpg")
    plt.cla()
    plt.clf()
    plt.close()


def plot_loss(task):
    make_folder("graphic")
    for j in ["loss", "val_loss"]:
        for i in range(1, 4):
            x = readTxt("model/"+task+"/model"+str(i)+"/"+j+".txt").flatten()
            plt.plot(x, label="model"+str(i))
        plt.xlabel("epoch")
        plt.ylabel(j)
        plt.title(task + "_"+j)
        plt.legend(loc='best')
        plt.savefig("graphic/"+task+"_"+j+".jpg")
        plt.cla()
        plt.clf()
        plt.close()


def plot_PCA(folder, task, model_num, event_num, layer_num=None):
    from sklearn.decomposition import PCA
    pca = PCA(2)
    make_folder("graphic")
    make_folder("graphic/"+task)

    if model_num == 1:
        model = model1()
    elif model_num == 2:
        model = model2()
    elif model_num == 3:
        model = model3()

    def p_norm(y_true, y_pred):
        grads = K.gradients(mean_squared_error(
            y_true, y_pred), model.trainable_weights)
        summed_squares = [K.sum(K.square(g)) for g in grads]
        norm = K.sqrt(sum(summed_squares))
        return norm
    model.compile(loss='mse',
                  optimizer='adam',
                  metrics=['accuracy', p_norm])

    for event in range(1, event_num+1):
        loss_origin = readTxt(
            folder+"/"+task+"/model"+str(model_num)+"/event"+str(event)+"/loss.txt").flatten()
        loss_step = loss_origin.shape[0]//20
        loss = ["{:.2e}".format(loss_origin[i*loss_step]) for i in range(20)]
        for x in range(20):
            model.load_weights(folder+"/"+task+"/model"+str(model_num) +
                               "/event"+str(event)+"/weights_epoch_{:0>2d}".format(x*3)+".hdf5")
            tmp = np.array([[]])

            if layer_num == None:
                for layer_weights in model.get_weights():
                    tmp = np.concatenate(
                        (tmp, layer_weights.reshape((1, -1))), axis=1)
            else:
                tmp = model.get_weights()[layer_num].reshape(1, -1)

            try:
                weights = np.vstack((weights, tmp))
            except:
                weights = tmp

    pca_weights = pca.fit_transform(weights)

    for event in range(1, event_num+1):
        x = pca_weights[:, 0][(event-1)*20:event*20]
        y = pca_weights[:, 1][(event-1)*20:event*20]
        plt.scatter(x, y)

        for i, txt in enumerate(loss):
            plt.annotate(txt, (x[i], y[i]))

    if layer_num == None:
        plt.title(task+"/PCA(model"+str(model_num)+")")
        plt.savefig("graphic/"+task+"/PCA(model"+str(model_num)+").jpg")
    else:
        plt.title(task+"/PCA(model"+str(model_num)+"_layer"+str(layer_num)+")")
        plt.savefig("graphic/"+task+"/PCA(model"+str(model_num) +
                    "_layer"+str(layer_num)+").jpg")

    plt.cla()
    plt.clf()
    plt.close()


def plot_Norm(task, model_num, event_num, iter=(0, 1000)):
    make_folder("graphic")
    make_folder("graphic/"+task)
    plt.figure(1)

    loss = readTxt("model/"+task+"/model"+str(model_num) +
                   "/event"+str(event_num)+"/loss.txt").flatten()
    grad = readTxt("model/"+task+"/model"+str(model_num) +
                   "/event"+str(event_num)+"/p_norm.txt").flatten()

    loss = loss[iter[0]:iter[1]]
    grad = grad[:1000]

    iteration = np.arange(1, loss.shape[0]+1)

    plt.subplot(2, 1, 1)
    plt.title(task+"/Norm(model"+str(model_num)+"_event"+str(event_num)+")")
    plt.plot(iteration, grad)
    plt.xlabel("iteration")
    plt.ylabel("grad")

    plt.subplot(2, 1, 2)
    plt.plot(iteration, loss)
    plt.xlabel("iteration")
    plt.ylabel("loss")

    plt.savefig("graphic/"+task+"/Norm(model" +
                str(model_num)+"_event"+str(event_num)+").jpg")

    plt.cla()
    plt.clf()
    plt.close()


if __name__ == '__main__':
    for model_num in range(1, 4):
        plot_Norm('sink', model_num=model_num, event_num=2, iter=(0, 1000))
        plot_Norm('square', model_num=model_num, event_num=2, iter=(0, 1000))
    """
    for event_num in range(1,7):
        plot_result_new("model","sink", event_num)
        plot_result_new("model","square", event_num)
    
    for model_num in range(1,4):
        #plot PCA of all layers
        plot_PCA("model","sink", model_num, event_num=6)
        plot_PCA("model","square", model_num, event_num=6)
        #plot PCA of layer0(layer_num=0)
        plot_PCA("model","sink", model_num, event_num=6, layer_num=0)
        plot_PCA("model","square", model_num, event_num=6, layer_num=0)
    
    """
