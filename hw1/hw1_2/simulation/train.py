import os

import keras.backend as K
import numpy as np
from keras.callbacks import Callback, LambdaCallback, ModelCheckpoint
from keras.layers import Dense
from keras.losses import mean_squared_error
from keras.models import Sequential

from dataSet import *
from readWrite import *


def model1():
    model1 = Sequential()
    model1.add(Dense(units=10000, activation='relu', input_dim=1))
    model1.add(Dense(units=1))
    return model1


def model2():
    model2 = Sequential()
    model2.add(Dense(units=85, activation='relu', input_dim=1))
    model2.add(Dense(units=86, activation='relu'))
    model2.add(Dense(units=86, activation='relu'))
    model2.add(Dense(units=86, activation='relu'))
    model2.add(Dense(units=85, activation='relu'))
    model2.add(Dense(units=1))
    return model2


def model3():
    model3 = Sequential()
    model3.add(Dense(units=58, activation='relu', input_dim=1))
    model3.add(Dense(units=57, activation='relu'))
    model3.add(Dense(units=57, activation='relu'))
    model3.add(Dense(units=57, activation='relu'))
    model3.add(Dense(units=57, activation='relu'))
    model3.add(Dense(units=57, activation='relu'))
    model3.add(Dense(units=57, activation='relu'))
    model3.add(Dense(units=57, activation='relu'))
    model3.add(Dense(units=57, activation='relu'))
    model3.add(Dense(units=57, activation='relu'))
    model3.add(Dense(units=1))
    return model3


def log_save(model, p_history, history, path):
    model.save(path+"/model.h5")
    writeTxt(p_history.loss_per_batch, path+"/loss.txt")
    writeTxt(history.history["val_loss"], path+"/val_loss.txt")
    writeTxt(p_history.p_norm, path+"/p_norm.txt")
    model.save_weights(save_path+"weights.hdf5")


class p_norm_History(Callback):
    def on_train_begin(self, logs={}):
        self.p_norm = []
        self.loss_per_batch = []

    def on_batch_end(self, batch, logs={}):
        self.p_norm.append(logs.get('p_norm'))
        self.loss_per_batch.append(logs.get('loss'))


if __name__ == '__main__':
    event_num = 6
    epochs = 60

    for i in ['sink', 'square']:
        # readdata
        x, y = readTrain("data/"+i+".csv")
        for model_num in range(1, 4):
            make_folder("model/"+i+"/model"+str(model_num))
            for event in range(1, event_num+1):
                # build folder
                save_path = "model/"+i+"/model" + \
                    str(model_num)+"/event"+str(event)+"/"
                make_folder(save_path)

                # model
                exec("model=model"+str(model_num)+"()")

                def p_norm(y_true, y_pred):
                    grads = K.gradients(mean_squared_error(
                        y_true, y_pred), model.trainable_weights)
                    summed_squares = [K.sum(K.square(g)) for g in grads]
                    norm = K.sqrt(sum(summed_squares))
                    return norm
                model.compile(loss='mse',
                              optimizer='adam',
                              metrics=[p_norm])

                # checkpoint
                p_history = p_norm_History()
                model.save_weights(save_path+"weights_epoch_00.hdf5")
                checkpoint = ModelCheckpoint(save_path+"weights_epoch_{epoch:02d}.hdf5",
                                             monitor='val_acc',
                                             verbose=0,
                                             save_weights_only=False,
                                             period=3)

                # fit
                history = model.fit(x, y,
                                    epochs=epochs,
                                    validation_split=0.2,
                                    callbacks=[checkpoint, p_history])

                log_save(model, p_history, history, save_path)
