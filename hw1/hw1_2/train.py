import os

import numpy as np
from keras.layers import Dense
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint, LambdaCallback ,Callback
import keras.backend as K
from keras.losses import mean_squared_error



from dataSet import *
from readWrite import *
from my_model import * 


def log_save(model, p_history, history, path):
    model.save(path+"/model.h5")
    writeTxt(p_history.loss_per_batch, path+"/loss.txt")
    writeTxt(history.history["val_loss"],path+"/val_loss.txt")
    writeTxt(p_history.p_norm, path+"/p_norm.txt")
    model.save_weights(save_path+"weights.hdf5")

class p_norm_History(Callback):
    def on_train_begin(self, logs={}):
        self.p_norm = []
        self.loss_per_batch = []

    def on_batch_end(self, batch, logs={}):
        self.p_norm.append(logs.get('p_norm'))
        self.loss_per_batch.append(logs.get('loss'))


if __name__ == '__main__':
    event_num = (1,10)
    epochs = 60

    for i in ['sinc', 'square']:
        #readdata
        x, y = readTrain("data/"+i+".csv")
        for model_num in range(1,4):
            make_folder("model/"+i+"/model"+str(model_num))
            for event in range(event_num[0],event_num[1]+1):
                #build folder
                save_path = "model/"+i+"/model"+str(model_num)+"/event"+str(event)+"/"
                make_folder(save_path)
                    
                #model
                exec("model=model"+str(model_num)+"()")
                def p_norm(y_true, y_pred):
                    grads = K.gradients(mean_squared_error(y_true, y_pred), model.trainable_weights)
                    summed_squares = [K.sum(K.square(g)) for g in grads]
                    norm = K.sqrt(sum(summed_squares))
                    return norm
                model.compile(loss='mse',
                              optimizer='adam',
                              metrics=[p_norm]) 

                #checkpoint
                p_history = p_norm_History()
                model.save_weights(save_path+"weights_epoch_00.hdf5")
                checkpoint = ModelCheckpoint(save_path+"weights_epoch_{epoch:02d}.hdf5", 
                                             monitor='val_acc',
                                             verbose=0,
                                             save_weights_only=False,
                                             period=3)

                #fit
                history = model.fit(x, y,
                                    epochs=epochs,
                                    validation_split=0.2,
                                    callbacks=[checkpoint, p_history])

                log_save(model, p_history, history, save_path)
