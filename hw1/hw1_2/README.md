# HW 1-2

* Author: 齊湣王/孟嘗君/馮諼

## Toolkits ##
* numpy 1.14.6
* tensorflow 1.8.0
* keras 2.1.5

## Regenerating ##
>Execute hw1_2/train.py -> this will RETRAIN models for PCA and Norm  
>Execute hw1_2/train_norm.py -> this will RETRAIN models for PCA and Norm  
>Execute hw1_2/graphic.py  -> this regenerates graphics only  
>The result is in hw1_2/graphic  

