import matplotlib.pyplot as plt
import numpy as np
import keras.backend as K
from keras.losses import mean_squared_error


from dataSet import *
from readWrite import *
from my_model import *

def model_select(index):
    if index == 1:
        return model1()
    elif index == 2:
        return model2()
    elif index == 3:
        return model3()
    elif index == 4:
        return model4()

def plot_result_new(folder, task, event_index):
    make_folder("graphic")
    make_folder("graphic/"+task)

    x, y = readData("data/"+task+".csv") 
    plt.plot(x, y, label="origin")
    for i in range(1, 4):
        model = model_select(i)
        def p_norm(y_true, y_pred):
            grads = K.gradients(mean_squared_error(y_true, y_pred), model.weights)
            summed_squares = [K.sum(K.square(g)) for g in grads]
            norm = K.sqrt(sum(summed_squares))
            return norm
        model.compile(loss='mse',
                      optimizer='adam',
                      metrics=['accuracy', p_norm]) 
        model.load_weights(folder+"/"+task+"/model"+str(i)+"/event"+str(event_index)+"/weights.hdf5")
        y = model.predict(x)
        plt.plot(x, y, label="model"+str(i))
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title(task+"/event"+str(event_index))
    plt.legend(loc='best')
    plt.savefig("graphic/"+task+"/predict(event"+str(event_index)+").jpg")
    plt.cla()
    plt.clf()
    plt.close()   

def plot_result(task, event_index):
    from keras.models import load_model
    make_folder("graphic")

    x, y = readData("data/"+task+".csv")

    plt.plot(x, y)
    for i in range(1, 4):
        model = load_model("model/"+task+"/model"+str(i)+"/event"+str(event_index)+"/model"+str(i)+".h5")
        y = model.predict(x)
        plt.plot(x, y, label="model"+str(i))
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title(task+"/event"+str(event_index))
    plt.legend(loc='best')
    plt.savefig("graphic/"+task+".jpg")
    plt.cla()
    plt.clf()
    plt.close()

def plot_loss(task):
    make_folder("graphic")
    for j in ["loss", "val_loss"]:
        for i in range(1, 4):
            x = readTxt("model/"+task+"/model"+str(i)+"/"+j+".txt").flatten()
            plt.plot(x, label="model"+str(i))
        plt.xlabel("epoch")
        plt.ylabel(j)
        plt.title(task + "_"+j)
        plt.legend(loc='best')
        plt.savefig("graphic/"+task+"_"+j+".jpg")
        plt.cla()
        plt.clf()
        plt.close()

def plot_PCA(folder, task, model_index, event_num,layer_index = None):
    from sklearn.decomposition import PCA
    pca = PCA(2)
    make_folder("graphic")
    make_folder("graphic/"+task)

    model = model_select(model_index)

    def p_norm(y_true, y_pred):
        grads = K.gradients(mean_squared_error(y_true, y_pred), model.trainable_weights)
        summed_squares = [K.sum(K.square(g)) for g in grads]
        norm = K.sqrt(sum(summed_squares))
        return norm
    model.compile(loss='mse',
                  optimizer='adam',
                  metrics=['accuracy', p_norm])

    for event in range(1, event_num+1):
        loss_origin = readTxt(folder+"/"+task+"/model"+str(model_index)+"/event"+str(event)+"/loss.txt").flatten()
        loss_step = loss_origin.shape[0]//20
        loss = ["{:.2e}".format(loss_origin[i*loss_step]) for i in range(20)]
        for x in range(event_num//3):
            model.load_weights(folder+"/"+task+"/model"+str(model_index)+"/event"+str(event)+"/weights_epoch_{:0>2d}".format(x*3)+".hdf5")
            tmp = np.array([[]])

            if layer_index == None:
                for layer_weights in  model.get_weights():
                    tmp = np.concatenate((tmp, layer_weights.reshape((1,-1))),axis=1)
            else:
                tmp = model.get_weights()[layer_index].reshape(1,-1)

            try:weights=np.vstack((weights,tmp))
            except:weights=tmp

    pca_weights=pca.fit_transform(weights)

    for event in range(1, event_num+1):
        x = pca_weights[:,0][(event-1)*20:event*20]
        y = pca_weights[:,1][(event-1)*20:event*20]
        plt.scatter(x, y)

        for i,txt in enumerate(loss): 
            try:plt.annotate(txt,(x[i],y[i]))
            except:pass     


    if layer_index==None:
        plt.title(task+"/PCA(model"+str(model_index)+")")
        plt.savefig("graphic/"+task+"/PCA(model"+str(model_index)+").jpg")
    else:
        plt.title(task+"/PCA(model"+str(model_index)+"_layer"+str(layer_index)+")")
        plt.savefig("graphic/"+task+"/PCA(model"+str(model_index)+"_layer"+str(layer_index)+").jpg")

    plt.cla()
    plt.clf()
    plt.close()   

def plot_Norm(task, model_index, event_index, iter=(0,1000)):
    make_folder("graphic")
    make_folder("graphic/"+task)
    plt.figure(1)

    loss = readTxt("model/"+task+"/model"+str(model_index)+"/event"+str(event_index)+"/loss.txt").flatten()
    grad = readTxt("model/"+task+"/model"+str(model_index)+"/event"+str(event_index)+"/p_norm.txt").flatten()
    
    loss = loss[iter[0]:iter[1]]
    grad = grad[:1000]
    
    iteration = np.arange(1, loss.shape[0]+1)

    plt.subplot(2,1,1)
    plt.title(task+"/Norm(model"+str(model_index)+"_event"+str(event_index)+")")
    plt.plot(iteration, grad)
    plt.xlabel("iteration")
    plt.ylabel("grad")

    plt.subplot(2,1,2)
    plt.plot(iteration, loss)
    plt.xlabel("iteration")
    plt.ylabel("loss")


    plt.savefig("graphic/"+task+"/Norm(model"+str(model_index)+"_event"+str(event_index)+").jpg")

    plt.cla()
    plt.clf()
    plt.close()   

def plot_minimum_ratio(task, model_index, event_num):
    import tensorflow as tf
    make_folder("graphic")
    make_folder("graphic/"+task)

    x, y = readData("data/"+task+".csv") 
    model = model_select(model_index)

    def p_norm(y_true, y_pred):
        grads = K.gradients(mean_squared_error(y_true, y_pred), model.weights)
        summed_squares = [K.sum(K.square(g)) for g in grads]
        norm = K.sqrt(sum(summed_squares))
        return norm
    model.compile(loss='mse',
                  optimizer='adam',
                  metrics=['accuracy', p_norm])

    loss=[]
    minimum_ratio=[]
    def random_array(data_size, reduced_size, seed=4):
        np.random.seed(seed)
        a=np.arange(data_size)
        np.random.shuffle(a)
        return sorted(a[:reduced_size])

    for event_index in range(1,event_num+1):
        model.load_weights("model_norm/"+task+"/model"+str(model_index)+"/event"+str(event_index)+"/best_model.hdf5")


        original_weights = model.get_weights()

        reduced_size=256
        random_range=random_array(y.shape[0], reduced_size=256)
        count1=0
        count2=0
        total=30

        original_loss = K.get_value(mean_squared_error(y[random_range].reshape((reduced_size,1)), model.predict(x[random_range])))

        for time in range(total):
            weights_list=[] 
            for weights in original_weights:
                weights_list.append(weights+(np.random.random(weights.shape)-0.5)/10e8)

            
            model.set_weights(weights_list)
            tmp = mean_squared_error(y[random_range].reshape((reduced_size,1)), model.predict(x[random_range]))
            tmp = K.get_value(tmp)
            count1 += np.sum(tmp>original_loss)
            count2 += np.sum(tmp<original_loss)
            
        minimum_ratio.append(count1/(count1+count2))
        loss.append(np.mean(original_loss))


    plt.scatter(minimum_ratio, loss)

    plt.xlabel('minimum_ratio')
    plt.ylabel('loss')
    plt.ylim((-max(loss)*0.1,max(loss)*1.1))
    plt.title(task+"/minimum_ratio(model"+str(model_index)+")")
    plt.savefig("graphic/"+task+"/minimum_ratio(model"+str(model_index)+").jpg")

    plt.cla()
    plt.clf()
    plt.close()   


if __name__ == '__main__':
    plot_minimum_ratio('sinc', model_index=4, event_num=10)

     
    #plot norm and loss
    for model_num in range(1,4):
        plot_Norm('sinc', model_index=model_num, event_index=1, iter=(0,1000)) 
        plot_Norm('square', model_index=model_num, event_index=1, iter=(0,1000))


    #plot function and predict
    for event_num in range(1,7):
        plot_result_new("model","sinc", event_num)
        plot_result_new("model","square", event_num)
    

    for model_num in range(1,4):
        #plot PCA of all layers
        plot_PCA("model","sinc", model_num, event_num=10)
        plot_PCA("model","square", model_num, event_num=10)

        #plot PCA of layer0(layer_num=0)
        plot_PCA("model","sinc", model_num, event_num=6, layer_index=0)
        plot_PCA("model","square", model_num, event_num=6, layer_index=0)


        
    
