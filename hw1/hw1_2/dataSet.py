from math import pi

import numpy as np

from readWrite import *


def write(f, path):
    x = np.arange(0, 1, 1e-4).reshape(1, -1) + 1e-4
    y = f(x)
    out = np.concatenate((x, y), axis=0).T
    writeTxt(out, path)


def sinc(x):
    return np.sin(5*pi*x)/(5*pi*x)


def square(x):
    result = np.zeros_like(x)
    result[x < 0.2] = 1
    result[np.logical_and(x > 0.4, x < 0.6)] = 1
    result[x > 0.8] = 1
    return result


def readData(path):
    data = readTxt(path)
    x = data[:, 0]
    y = data[:, 1]
    return x, y


def readTrain(path):
    x, y = readData(path)
    dataNum = x.shape[0]
    if os.path.isfile('data/permutation.txt'):
        p = readTxt('data/permutation.txt').flatten()
        p = np.array(p, dtype='int')
    else:
        p = np.random.permutation(dataNum)
        make_folder('data')
        writeTxt(p, 'data/permutation.txt')

    return x[p], y[p]


if __name__ == '__main__':
    write(sinc, "data/sinc.csv")    
    write(square, "data/square.csv")
