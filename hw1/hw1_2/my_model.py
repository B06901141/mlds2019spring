from keras.models import Sequential
from keras.layers import Dense

def model1():
    model = Sequential()
    model.add(Dense(units=10000, activation='relu', input_dim=1))
    model.add(Dense(units=1)) 
    return model

def model2():
    model = Sequential()
    model.add(Dense(units=85, activation='relu', input_dim=1))
    model.add(Dense(units=86, activation='relu'))
    model.add(Dense(units=86, activation='relu'))
    model.add(Dense(units=86, activation='relu'))
    model.add(Dense(units=85, activation='relu'))
    model.add(Dense(units=1))
    return model

def model3():
    model = Sequential()
    model.add(Dense(units=58, activation='relu', input_dim=1))
    model.add(Dense(units=57, activation='relu'))
    model.add(Dense(units=57, activation='relu'))
    model.add(Dense(units=57, activation='relu'))
    model.add(Dense(units=57, activation='relu'))
    model.add(Dense(units=57, activation='relu'))
    model.add(Dense(units=57, activation='relu'))
    model.add(Dense(units=57, activation='relu'))
    model.add(Dense(units=57, activation='relu'))
    model.add(Dense(units=57, activation='relu'))
    model.add(Dense(units=1))
    return model

def model4():
    model = Sequential()
    model.add(Dense(units=32, activation='relu', input_dim=1))
    model.add(Dense(units=32, activation='relu'))
    model.add(Dense(units=32, activation='relu'))
    model.add(Dense(units=32, activation='relu'))
    model.add(Dense(units=32, activation='relu'))
    model.add(Dense(units=1)) 
    return model
